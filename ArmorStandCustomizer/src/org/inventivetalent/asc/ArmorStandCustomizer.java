/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.inventivetalent.asc.animator.AnimationManager;
import org.inventivetalent.asc.command.CommandRegistry;
import org.inventivetalent.asc.command.animate.ArmorStandAnimate;
import org.inventivetalent.asc.command.clone.ArmorStandClone;
import org.inventivetalent.asc.command.create.ArmorStandCreate;
import org.inventivetalent.asc.command.delete.ArmorStandDelete;
import org.inventivetalent.asc.command.edit.ArmorStandEdit;
import org.inventivetalent.asc.command.find.ArmorStandFind;
import org.inventivetalent.asc.command.flip.ArmorStandFlip;
import org.inventivetalent.asc.command.help.ArmorStandHelp;
import org.inventivetalent.asc.command.info.ArmorStandInfo;
import org.inventivetalent.asc.command.input.ArmorStandInput;
import org.inventivetalent.asc.command.list.ArmorStandList;
import org.inventivetalent.asc.command.load.ArmorStandLoad;
import org.inventivetalent.asc.command.move.ArmorStandMove;
import org.inventivetalent.asc.command.name.ArmorStandName;
import org.inventivetalent.asc.command.particles.ArmorStandParticles;
import org.inventivetalent.asc.command.publish.ArmorStandPublish;
import org.inventivetalent.asc.command.redo.ArmorStandRedo;
import org.inventivetalent.asc.command.rotate.ArmorStandRotate;
import org.inventivetalent.asc.command.save.ArmorStandSave;
import org.inventivetalent.asc.command.save.ArmorStandSaveSelection;
import org.inventivetalent.asc.command.selection.ArmorStandSelection;
import org.inventivetalent.asc.command.slot.ArmorStandSlot;
import org.inventivetalent.asc.command.teleport.ArmorStandTeleport;
import org.inventivetalent.asc.command.undo.ArmorStandUndo;
import org.inventivetalent.asc.config.ConfigManager;
import org.inventivetalent.asc.data.DataManager;
import org.inventivetalent.asc.editor.EditListener;
import org.inventivetalent.asc.packet.PacketListener;
import org.inventivetalent.asc.session.SessionListener;
import org.inventivetalent.asc.session.SessionManager;
import org.inventivetalent.asc.stand.ArmorStandListener;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.task.TaskManager;
import org.inventivetalent.asc.timings.TimingsManager;
import org.inventivetalent.asc.worldedit.WorldEditManager;
import org.inventivetalent.packetlistener.handler.PacketHandler;
import org.mcstats.MetricsLite;

import java.util.logging.Level;

public class ArmorStandCustomizer extends JavaPlugin {

	private static ArmorStandCustomizer instance;
	private static String prefix = "§b[§2AS§6C§b]§r ";

	private SessionManager    sessionManager;
	private ArmorStandManager armorStandManager;
	private AnimationManager  animationManager;
	private WorldEditManager  worldEditManager;
	private TaskManager       taskManager;
	private ConfigManager     configManager;

	private DataManager dataManager;

	private TimingsManager timingsManager;

	private PacketListener packetListener;

	private String hostName    = "";
	private String hostAddress = "";

	@Override
	public void onEnable() {
		instance = this;

		try {
			Class.forName("org.bukkit.entity.ArmorStand");
		} catch (Exception e) {
			getLogger().severe("Invalid server version!");
			getLogger().severe("Please use 1.8+");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		try {
			Class.forName("org.bukkit.Server$Spigot");
		} catch (Exception e) {
			getLogger().warning("Server does not seem to be spigot.");
			getLogger().warning("This plugin was developed for spigot servers and might not work as intended on other implementations.");
			getLogger().warning("Issues on non-spigot servers will probably be ignored.");
		}

		if (!checkDependencies()) {
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		timingsManager = new TimingsManager(this);

		//Managers
		taskManager = new TaskManager(this);
		sessionManager = new SessionManager(this);
		armorStandManager = new ArmorStandManager(this);
		animationManager = new AnimationManager(this);
		worldEditManager = new WorldEditManager(this);
		configManager = new ConfigManager(this);

		dataManager = new DataManager(this);

		Bukkit.getScheduler().runTaskLater(this, new Runnable() {
			@Override
			public void run() {
				//Data
				getLogger().info("Loading data...");

				boolean loadSuccess = true;
				try {
					if (!dataManager.loadArmorStands()) {
						loadSuccess = false;
					}

					if (!dataManager.loadAnimations()) {
						loadSuccess = false;
					}
				} catch (Throwable e) {
					e.printStackTrace();
				}

				if (!loadSuccess) {
					getLogger().severe("Failed to load data");
					if (isEnabled()) { Bukkit.getPluginManager().disablePlugin(ArmorStandCustomizer.this); }
					return;
				}
			}
		}, 20);

		PacketHandler.addHandler(packetListener = new PacketListener(this));

		// Listeners
		Bukkit.getPluginManager().registerEvents(new SessionListener(), this);
		Bukkit.getPluginManager().registerEvents(new EditListener(), this);
		Bukkit.getPluginManager().registerEvents(new ArmorStandListener(), this);

		//Commands
		CommandRegistry.register("armorstandcustomizer", org.inventivetalent.asc.command.base.ArmorStandCustomizer.class);
		CommandRegistry.register("armorstandcreate", ArmorStandCreate.class);
		CommandRegistry.register("armorstandedit", ArmorStandEdit.class);
		CommandRegistry.register("armorstandslot", ArmorStandSlot.class);
		CommandRegistry.register("armorstandteleport", ArmorStandTeleport.class);
		CommandRegistry.register("armorstandlist", ArmorStandList.class);
		CommandRegistry.register("armorstandfind", ArmorStandFind.class);
		CommandRegistry.register("armorstandsave", ArmorStandSave.class);
		CommandRegistry.register("armorstandsaveselection", ArmorStandSaveSelection.class);
		CommandRegistry.register("armorstandload", ArmorStandLoad.class);
		CommandRegistry.register("armorstanddelete", ArmorStandDelete.class);
		CommandRegistry.register("armorstandselection", ArmorStandSelection.class);
		CommandRegistry.register("armorstandmove", ArmorStandMove.class);
		CommandRegistry.register("armorstandclone", ArmorStandClone.class);
		CommandRegistry.register("armorstandname", ArmorStandName.class);
		CommandRegistry.register("armorstandinfo", ArmorStandInfo.class);
		CommandRegistry.register("armorstandanimate", ArmorStandAnimate.class);
		CommandRegistry.register("armorstandhelp", ArmorStandHelp.class);
		CommandRegistry.register("armorstandpublish", ArmorStandPublish.class);
		CommandRegistry.register("armorstandundo", ArmorStandUndo.class);
		CommandRegistry.register("armorstandredo", ArmorStandRedo.class);
		CommandRegistry.register("armorstandrotate", ArmorStandRotate.class);
		CommandRegistry.register("armorstandflip", ArmorStandFlip.class);
		CommandRegistry.register("armorstandparticles", ArmorStandParticles.class);
		CommandRegistry.register("armorstandinput", ArmorStandInput.class);

		//Tasks
		taskManager.startAllTasks();

		try {
			MetricsLite metrics = new MetricsLite(this);
			if (metrics.start()) {
				getLogger().info("Metrics started");
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onDisable() {
		PacketHandler.removeHandler(packetListener);

		//Data
		getLogger().info("Saving data...");

		boolean saveSuccess = true;
		if (!dataManager.saveArmorStands()) {
			saveSuccess = false;
		}

		if (!dataManager.saveAnimations()) {
			saveSuccess = false;
		}

		if (!saveSuccess) {
			getLogger().severe("Failed to save data");
		}

		instance = null;
	}

	public static ArmorStandCustomizer getInstance() {
		return instance;
	}

	public static String getPrefix() {
		return prefix;
	}

	public SessionManager getSessionManager() {
		return sessionManager;
	}

	public ArmorStandManager getArmorStandManager() {
		return armorStandManager;
	}

	public AnimationManager getAnimationManager() {
		return animationManager;
	}

	public WorldEditManager getWorldEditManager() {
		return worldEditManager;
	}

	public TaskManager getTaskManager() {
		return taskManager;
	}

	public ConfigManager getConfigManager() {
		return configManager;
	}

	public DataManager getDataManager() {
		return dataManager;
	}

	public TimingsManager getTimingsManager() {
		return timingsManager;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	protected boolean checkDependencies() {
		if (!Bukkit.getPluginManager().isPluginEnabled("PacketListenerApi")) {
			getLogger().severe("****************************************");
			getLogger().severe(" ");
			getLogger().severe("This plugin depends on PacketListenerAPI");
			getLogger().severe("https://www.spigotmc.org/resources/2930/");
			getLogger().severe(" ");
			getLogger().severe("****************************************");
			return false;
		}

		if (!useWorldEdit()) {
			getLogger().warning(" ");
			getLogger().warning("  WorldEdit is recommended to enable all features  ");
			getLogger().warning("  http://dev.bukkit.org/bukkit-plugins/worldedit/  ");
			getLogger().warning(" ");
		}

		return true;
	}

	public static boolean useWorldEdit() {
		return Bukkit.getPluginManager().isPluginEnabled("WorldEdit");
	}

	public static <T extends Plugin> T getDependency(String name, Class<T> clazz) {
		try {
			Plugin plugin = Bukkit.getPluginManager().getPlugin(name);
			if (clazz.isInstance(plugin)) {
				//noinspection unchecked
				return (T) plugin;
			} else {
				getInstance().getLogger().warning("Found dependency \"" + name + "\", but it's not an instance of " + clazz.getSimpleName());
			}
		} catch (Exception e) {
			getInstance().getLogger().log(Level.WARNING, "Could not get dependency \"" + name + "\"", e);
		}
		return null;
	}

}
