/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.task;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.session.SessionManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.particle.ParticleEffect;

import java.util.*;

public class AxisParticles implements Runnable {

	public static long DELAY    = 5;
	public static long INTERVAL = 5;

	Plugin plugin;

	List<UUID> queue = new ArrayList<>();

	public AxisParticles(Plugin plugin) {
		this.plugin = plugin;
	}

	public void add(Session session) {
		this.queue.add(session.getPlayer().getUniqueId());
	}

	@Override
	public void run() {
		if (queue.isEmpty()) { return; }

		ListIterator<UUID> iterator = queue.listIterator();

		SessionManager manager = ArmorStandCustomizer.getInstance().getSessionManager();

		while (iterator.hasNext()) {
			UUID uuid = iterator.next();

			Session session = manager.getSession(Bukkit.getOfflinePlayer(uuid));

			if (session != null) {
				if (session.isEditing() && session.hasParticles()) {
					ArmorStandWrapper wrapper = session.getEditing();
					Player player = session.getPlayer().getPlayer();

					Vector center = wrapper.getWrapped().getLocation().toVector();

					Vector base = new Vector(0, 0, 0);
					base.add(center);

					//Origin
					displayParticle(player, Color.BLACK, base);

					//X-Axis
					for (double x = -2; x < 2; x += 0.25) {
						displayParticle(player, Color.RED, base.clone().add(new Vector(x, 0, 0)));
					}
					displayParticle(player, Color.RED, base.clone().add(new Vector(2.5, 0, 0)));

					//Z-Axis
					for (double z = -2; z < 2; z += 0.25) {
						displayParticle(player, Color.BLUE, base.clone().add(new Vector(0, 0, z)));
					}
					displayParticle(player, Color.BLUE, base.clone().add(new Vector(0, 0, 2.5)));

					//Y-Axis
					for (double y = -2; y < 4; y += 0.25) {
						displayParticle(player, Color.GREEN, base.clone().add(new Vector(0, y, 0)));
					}
					displayParticle(player, Color.GREEN, base.clone().add(new Vector(0, 4.5, 0)));
				}
			} else {
				iterator.remove();
			}
		}
	}

	public void displayParticle(Player player, Color color, Vector vector) {
		if (player == null || vector == null) { return; }
		try {
			Location location = vector.toLocation(player.getWorld());
			ParticleEffect.REDSTONE.sendColor(Collections.singleton(player), location.getX(), location.getY(), location.getZ(), color);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}


