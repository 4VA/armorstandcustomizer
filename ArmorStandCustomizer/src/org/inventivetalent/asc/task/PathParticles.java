/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.task;

import org.inventivetalent.particle.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.animator.Animation;
import org.inventivetalent.asc.animator.Animator;
import org.inventivetalent.asc.animator.Frame;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.session.SessionManager;

import java.util.*;

public class PathParticles implements Runnable {

	public static long DELAY    = 5;
	public static long INTERVAL = 5;

	Plugin plugin;

	List<UUID> queue = new ArrayList<>();

	public PathParticles(Plugin plugin) {
		this.plugin = plugin;
	}

	public void add(Session session) {
		this.queue.add(session.getPlayer().getUniqueId());
	}

	@Override
	public void run() {
		if (queue.isEmpty()) { return; }

		ListIterator<UUID> iterator = queue.listIterator();

		SessionManager manager = ArmorStandCustomizer.getInstance().getSessionManager();

		while (iterator.hasNext()) {
			UUID uuid = iterator.next();

			Session session = manager.getSession(Bukkit.getOfflinePlayer(uuid));

			if (session != null) {
				Player player = session.getPlayer().getPlayer();

				if (session.isAnimating()&&session.hasParticles()) {
					Animator animator = session.getAnimator();
					Animation animation = animator.getAnimation();
					if (animation == null) { continue; }

					if (animation.getLength() == 0) {
						continue;
					}

					for (int i = 0; i < animation.getLength(); i++) {
						Frame frame = animation.getFrame(i);

						Location location = frame.getLocation();

						displayParticle(player, Color.YELLOW, location.toVector());
					}
				}
			} else {
				iterator.remove();
			}
		}
	}

	public void displayParticle(Player player, Color color, Vector vector) {
		if (player == null || vector == null) { return; }
		try {
			Location location=vector.toLocation(player.getWorld());
			ParticleEffect.REDSTONE.sendColor(Collections.singleton(player), location.getX(),location.getY(),location.getZ(), color);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
