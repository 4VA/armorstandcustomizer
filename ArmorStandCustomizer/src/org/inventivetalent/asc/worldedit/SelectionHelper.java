/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.worldedit;

import com.sk89q.worldedit.math.transform.AffineTransform;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public class SelectionHelper {

	// From: https://github.com/sk89q/WorldEdit/blob/master/worldedit-core/src/main/java/com/sk89q/worldedit/command/ClipboardCommands.java#L176
	public static Vector rotateVector(Vector vec, EulerAngle angle) {
		AffineTransform transform = new AffineTransform();
		transform = transform.rotateY(-angle.getY());
		transform = transform.rotateX(-angle.getX());
		transform = transform.rotateZ(-angle.getZ());

		com.sk89q.worldedit.Vector rot = transform.apply(toWEVector(vec));

		return toBukkitVector(rot);
	}

	// From: https://github.com/sk89q/WorldEdit/blob/master/worldedit-core/src/main/java/com/sk89q/worldedit/command/ClipboardCommands.java#L210
	public static Vector flipVector(Vector vec, Vector direction) {
		com.sk89q.worldedit.Vector weVector = toWEVector(direction);
		AffineTransform transform = new AffineTransform();
		transform = transform.scale(weVector.positive().multiply(-2).add(1, 1, 1));

		com.sk89q.worldedit.Vector flip = transform.apply(toWEVector(vec));

		return toBukkitVector(flip);
	}

	public static com.sk89q.worldedit.Vector toWEVector(Vector bukkit) {
		return new com.sk89q.worldedit.Vector(bukkit.getX(), bukkit.getY(), bukkit.getZ());
	}

	public static Vector toBukkitVector(com.sk89q.worldedit.Vector we) {
		return new Vector(we.getX(), we.getY(), we.getZ());
	}

}
