/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.worldedit;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.util.JSONSerializer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class WorldEditManager {

	private ArmorStandCustomizer plugin;
	private boolean enabled = false;

	public WorldEditManager(ArmorStandCustomizer plugin) {
		this.plugin = plugin;
		this.enabled = ArmorStandCustomizer.useWorldEdit();
	}

	public boolean hasSelection(Player player) {
		if (!enabled) {
			return false;
		}
		WorldEditPlugin worldEdit = ArmorStandCustomizer.getDependency("WorldEdit", WorldEditPlugin.class);

		return worldEdit != null && worldEdit.getSelection(player) != null;
	}

	public <T extends Entity> Collection<T> getEntitiesInSelection(Player player, Class<T> clazz) {
		List<T> list = new ArrayList<>();
		if (!hasSelection(player)) {
			return list;
		}
		WorldEditPlugin worldEdit = ArmorStandCustomizer.getDependency("WorldEdit", WorldEditPlugin.class);
		Selection selection = worldEdit.getSelection(player);

		Collection<T> entities = selection.getWorld().getEntitiesByClass(clazz);
		Iterator<T> iterator = entities.iterator();

		while (iterator.hasNext()) {
			T next = iterator.next();

			if (selection.contains(next.getLocation())) {
				list.add(next);
			}
		}

		return list;
	}

	public List<ArmorStandWrapper> getArmorStandsInSelection(Player player) {
		List<ArmorStandWrapper> list = new ArrayList<>();
		if (!hasSelection(player)) {
			return list;
		}

		ArmorStandManager armorStandManager = ArmorStandCustomizer.getInstance().getArmorStandManager();

		Collection<ArmorStand> stands = getEntitiesInSelection(player, ArmorStand.class);
		for (ArmorStand stand : stands) {
			UUID uuid = stand.getUniqueId();
			if (armorStandManager.isRegistered(uuid)) {
				String identifier = armorStandManager.getIdentifier(uuid);

				list.add(armorStandManager.getArmorStand(player.getWorld(), uuid, identifier));
			}
		}

		return list;
	}

	public JSONObject serializeSelection(Player player) {
		JSONObject json = new JSONObject();
		if (!hasSelection(player)) {
			return json;
		}
		WorldEditPlugin worldEdit = ArmorStandCustomizer.getDependency("WorldEdit", WorldEditPlugin.class);
		if (worldEdit == null) {
			return json;
		}
		final Selection selection = worldEdit.getSelection(player);

		JSONArray blocks = getBlocksInRegion(selection.getMinimumPoint(), selection.getMaximumPoint(), player.getLocation());

		json.put("dimension", new JSONObject() {{
			put("width", selection.getWidth());
			put("length", selection.getLength());
			put("height", selection.getHeight());
		}});
		json.put("blocks", blocks);

		return json;
	}

	public void deserializeSelection(Player player, JSONObject json, boolean ignoreAir) {
		World world = player.getWorld();

		JSONArray blocks = json.getJSONArray("blocks");

		List<Integer> ignoredList = new ArrayList<>();
		if (ignoreAir) {
			ignoredList.add(0);
		}
		Integer[] ignored = ignoredList.toArray(new Integer[ignoredList.size()]);

		for (int i = 0; i < blocks.length(); i++) {
			JSONObject current = blocks.getJSONObject(i);

			JSONSerializer.JSONToBlock(current, world, player.getLocation(), ignored);
		}
	}

	public JSONArray getBlocksInRegion(Location min, Location max, Location origin) {
		if (min == null || max == null) {
			throw new IllegalArgumentException();
		}
		if (!min.getWorld().equals(max.getWorld())) {
			throw new IllegalArgumentException();
		}

		JSONArray array = new JSONArray();

		int minX = min.getBlockX();
		int minY = min.getBlockY();
		int minZ = min.getBlockZ();

		int maxX = max.getBlockX();
		int maxY = max.getBlockY();
		int maxZ = max.getBlockZ();

		for (int x = minX; x < maxX; x++) {
			for (int z = minZ; z < maxZ; z++) {
				for (int y = minY; y < maxY; y++) {
					Location location = new Location(min.getWorld(), x, y, z);
					Block block = location.getBlock();

					JSONObject serialized = JSONSerializer.BlockToJSON(block, origin);

					array.put(serialized);
				}
			}
		}

		return array;
	}

	public org.bukkit.util.Vector getMin(Player player) {
		if (!hasSelection(player)) {
			return new Vector();
		}
		WorldEditPlugin worldEdit = ArmorStandCustomizer.getDependency("WorldEdit", WorldEditPlugin.class);
		if (worldEdit == null) {
			return new Vector();
		}

		return worldEdit.getSelection(player).getMinimumPoint().toVector();
	}

	public org.bukkit.util.Vector getMax(Player player) {
		if (!hasSelection(player)) {
			return new Vector();
		}
		WorldEditPlugin worldEdit = ArmorStandCustomizer.getDependency("WorldEdit", WorldEditPlugin.class);
		if (worldEdit == null) {
			return new Vector();
		}

		return worldEdit.getSelection(player).getMaximumPoint().toVector();
	}

}
