/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.editor;

import org.bukkit.entity.Player;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandPart;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.undo.MovementUndoable;
import org.inventivetalent.asc.undo.RotationUndoable;

public class Editor {

	private final Session session;

	public Editor(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return session;
	}

	public void handleInput(Player player, boolean increase, boolean decrease, boolean sneaking) {
		if (!getSession().isEditing()) {
			return;
		}
		if (player == null) {
			throw new IllegalArgumentException("player cannot be null");
		}
		if (increase == decrease) {
			throw new IllegalArgumentException("increase and decrease cannot both be true or false");
		}

		ArmorStandWrapper editing = getSession().getEditing();
		ArmorStandPart part = getSession().getEditingPart();

		double moveSteps = getSession().getMoveSteps();
		double rotationSteps = getSession().getRotationSteps();

		EditAxis axis = EditAxis.getForPlayer(player);

		//Reverse the action based on the direction
		if (axis.isNegative()) {
			increase = !increase;
			decrease = !decrease;
		}

		if (sneaking) {
			axis = axis.withRotation(editing.getWrapped().getLocation().getYaw());

			EulerAngle mod = EulerAngle.ZERO;
			mod = axis.applyToAngle(mod, rotationSteps);

			if (session.getRotationMode() == RotationMode.BODY && axis == EditAxis.Y) {
				float steps = (float) (increase ? rotationSteps : -rotationSteps);
				session.trackUndo(new RotationUndoable(editing, steps));
				editing.rotateRelative(steps);
			} else {
				RotationUndoable undoable = new RotationUndoable(editing, part, editing.getPose(part));
				session.trackUndo(undoable);
				if (increase) {
					editing.addPose(part, mod);
				} else {
					editing.subtractPose(part, mod);
				}
				undoable.newAngle = editing.getPose(part);
			}
		} else {
			Vector mod = new Vector();
			mod = axis.applyToVector(mod, moveSteps);

			if (increase) {
				session.trackUndo(new MovementUndoable(editing, mod));
				editing.moveRelative(mod);
			} else {
				mod = mod.multiply(-1);
				session.trackUndo(new MovementUndoable(editing, mod));
				editing.moveRelative(mod);
			}
		}
	}

	//Just a small method to keep the code a bit cleaner
	private double calculate(double a, double b, boolean increase, boolean decrease) {
		if (increase) {
			return a + b;
		}
		if (decrease) {
			return a - b;
		}
		return a;
	}

}

