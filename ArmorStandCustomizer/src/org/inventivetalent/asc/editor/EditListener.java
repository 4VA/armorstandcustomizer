/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.editor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.session.SessionManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;

public class EditListener implements Listener {

	private int SCROLL_MIN = 1;
	private int SCROLL_MAX = 8;

	@EventHandler
	public void onScroll(PlayerItemHeldEvent e) {
		final Player player = e.getPlayer();

		int from = e.getPreviousSlot();
		int to = e.getNewSlot();

		SessionManager manager = ArmorStandCustomizer.getInstance().getSessionManager();
		Session session = null;
		if ((session = manager.getSession(player)) == null) {
			return;
		}

		if (session.getInputMode() != InputMode.SCROLL) { return; }

		ArmorStandWrapper editing = null;
		if ((editing = session.getEditing()) == null) {
			return;
		}

		if (from >= SCROLL_MAX && to <= SCROLL_MIN) {
			to = 9;
		}
		if (from <= SCROLL_MIN && to >= SCROLL_MAX) {
			to = -1;
		}

		final boolean decrease = to > from;
		final boolean increase = to < from;

		final Editor editor = session.getEditor();
		if (editor == null) {
			throw new IllegalStateException(player.getName() + " has no editor instance but is editing " + editing);
		}

		Bukkit.getScheduler().runTaskAsynchronously(ArmorStandCustomizer.getInstance(), new Runnable() {
			@Override
			public void run() {
				editor.handleInput(player, increase, decrease, player.isSneaking());
			}
		});
	}

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		final Player player = event.getPlayer();

		Location from = event.getFrom();
		Location to = event.getTo();

		if (locationEqual(from, to)) {
			return;
		}

		SessionManager manager = ArmorStandCustomizer.getInstance().getSessionManager();
		Session session = null;
		if ((session = manager.getSession(player)) == null) {
			return;
		}

		if (session.getInputMode() != InputMode.MOVE) { return; }

		ArmorStandWrapper editing = null;
		if ((editing = session.getEditing()) == null) {
			return;
		}

		EditAxis axis = EditAxis.getForPlayer(player);

		Vector mod = axis.applyToVector(new Vector(), 1);

		Vector fromVector = from.toVector().multiply(mod);
		Vector toVector = to.toVector().multiply(mod);

		double fromValue = axis == EditAxis.X ? fromVector.getX() : axis == EditAxis.Y ? fromVector.getY() : axis == EditAxis.Z ? fromVector.getZ() : 0;
		double toValue = axis == EditAxis.X ? toVector.getX() : axis == EditAxis.Y ? toVector.getY() : axis == EditAxis.Z ? toVector.getZ() : 0;

		final boolean decrease = toValue > fromValue;
		final boolean increase = toValue < fromValue;

		if (decrease == increase) { return; }

		final Editor editor = session.getEditor();
		if (editor == null) {
			throw new IllegalStateException(player.getName() + " has no editor instance but is editing " + editing);
		}

		Bukkit.getScheduler().runTaskAsynchronously(ArmorStandCustomizer.getInstance(), new Runnable() {
			@Override
			public void run() {
				editor.handleInput(player, increase, decrease, player.isSneaking());
			}
		});
	}

	boolean locationEqual(Location a, Location b) {
		return a.getX() == b.getX() && a.getY() == b.getY() && a.getZ() == b.getZ();
	}

	@EventHandler
	public void onDropItem(PlayerDropItemEvent e) {
		Player player = e.getPlayer();

		SessionManager manager = ArmorStandCustomizer.getInstance().getSessionManager();
		Session session = null;
		if ((session = manager.getSession(player)) == null) {
			return;
		}

		player.sendMessage(" ");
		if (EditAxis.getForPlayer(player) == EditAxis.Y) {
			session.switchRotationMode();
		} else {
			session.cycleSteps(player.isSneaking());
		}
		e.setCancelled(true);
		player.updateInventory();
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();

		if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
			SessionManager manager = ArmorStandCustomizer.getInstance().getSessionManager();
			Session session = null;
			if ((session = manager.getSession(player)) == null) {
				return;
			}

			if (session.isEditing()) {
				session.stopEditing(false);
			} else {
				session.stopEditing(true);
			}
			e.setCancelled(true);
		}
	}

	public void sendMessage(CommandSender receiver, String message, String... format) {
		try {
			message = String.format(message, format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		receiver.sendMessage(ArmorStandCustomizer.getPrefix() + message);
	}

}
