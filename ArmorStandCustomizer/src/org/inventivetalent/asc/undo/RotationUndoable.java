/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.undo;

import org.bukkit.util.EulerAngle;
import org.inventivetalent.asc.stand.ArmorStandPart;
import org.inventivetalent.asc.stand.ArmorStandWrapper;

public class RotationUndoable implements Undoable {

	final ArmorStandWrapper wrapper;

	// If it's a part rotation
	final ArmorStandPart part;
	final EulerAngle     angle;

	// If it is a body rotation instead
	final float rotation;

	//Track for redo
	public EulerAngle newAngle;

	public RotationUndoable(ArmorStandWrapper wrapper, ArmorStandPart part, EulerAngle rotation) {
		this.wrapper = wrapper;
		this.part = part;
		this.angle = rotation;
		this.rotation = 0;
	}

	public RotationUndoable(ArmorStandWrapper wrapper, float rotation) {
		this.wrapper = wrapper;
		this.rotation = rotation;
		this.part = null;
		this.angle = null;
	}

	@Override
	public void undo() {
		if (angle != null && part != null) {
			wrapper.setPose(part, angle);
		} else {
			wrapper.rotateRelative(-rotation);
		}
	}

	@Override
	public void redo() {
		if (angle != null && part != null) {
			wrapper.setPose(part, newAngle);
		} else {
			wrapper.rotateRelative(rotation);
		}
	}
}
