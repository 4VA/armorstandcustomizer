/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.undo;

import org.inventivetalent.asc.ArmorStandCustomizer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class UndoTracker {

	private final List<Undoable> actions = new ArrayList<>();
	private final List<Undoable> undone  = new ArrayList<>();

	private boolean locked = false;// To prevent access to the list while undoing (Also to make sure the undone actions are not tracked again)

	public void track(Undoable undoable) {
		if (!locked) { actions.add(undoable); }
	}

	public boolean undo(int amount) {
		locked = true;
		for (int i = 0; i < amount; i++) {
			if (actions.isEmpty()) {
				locked = false;
				return false;
			}
			Undoable current = actions.remove(actions.size() - 1);
			try {
				current.undo();
				undone.add(current);
			} catch (Throwable throwable) {
				ArmorStandCustomizer.getInstance().getLogger().log(Level.SEVERE, "Failed to execute undo for " + current, throwable);
			}
		}
		locked = false;
		return true;
	}

	public boolean redo(int amount) {
		locked = true;
		for (int i = 0; i < amount; i++) {
			if (undone.isEmpty()) {
				locked = false;
				return false;
			}
			Undoable current = undone.remove(undone.size() - 1);
			try {
				current.redo();
				actions.add(current);
			} catch (Throwable throwable) {
				ArmorStandCustomizer.getInstance().getLogger().log(Level.SEVERE, "Failed to execute redo for " + current, throwable);
			}
		}
		locked = false;
		return true;
	}

	public int undoable() {
		return actions.size();
	}

	public int redoable() {
		return undone.size();
	}

}
