/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.data;

import java.io.File;

public class FileResult<T> {

	public enum Result {
		SUCCESS,
		INVALID_FILE,
		FAILURE
	}

	private Result result;
	private File   file;
	private String relativeFile;
	private String fileName;
	private T      content;

	public FileResult(Result result, File changed) {
		this(result, changed, null);
	}

	public FileResult(Result result, File file, T content) {
		this.result = result;
		this.file = file;
		this.content = content;

		this.relativeFile = getFile().toString();
		int index = relativeFile.lastIndexOf("plugins");
		if (index != -1) { relativeFile = relativeFile.substring(index); }

		fileName = getFile().getName();
		fileName = fileName.substring(0, fileName.lastIndexOf('.'));
	}

	public Result getResult() {
		return result;
	}

	public T getContent() {
		return content;
	}

	public File getFile() {
		return file;
	}

	public String getRelativeFile() {
		return relativeFile;
	}

	public String getFileName() {
		return fileName;
	}
}
