/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.data;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.animator.Animation;
import org.inventivetalent.asc.animator.AnimationManager;
import org.inventivetalent.asc.config.ConfigManager;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.task.Callback;
import org.inventivetalent.asc.timings.DataTimings;
import org.inventivetalent.asc.worldedit.WorldEditManager;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataManager {

	private ArmorStandCustomizer plugin;

	private File pluginFolder;
	private File dataFolder;
	private File armorStandFile;
	private File animationFile;
	private File savesFolder;

	ArmorStandManager armorStandManager;
	AnimationManager  animationManager;

	DataTimings timings;

	public DataManager(ArmorStandCustomizer plugin) {
		this.plugin = plugin;

		if (!initFiles()) {
			plugin.getLogger().severe("Failed to initialize data files");
			Bukkit.getPluginManager().disablePlugin(plugin);
			return;
		}

		armorStandManager = plugin.getArmorStandManager();
		animationManager = plugin.getAnimationManager();

		timings = ArmorStandCustomizer.getInstance().getTimingsManager().getDataTimings();
	}

	private boolean initFiles() {
		pluginFolder = plugin.getDataFolder();
		if (!pluginFolder.exists()) {
			if (!pluginFolder.mkdirs()) {
				return false;
			}
		}

		dataFolder = new File(pluginFolder, "data");
		if (!dataFolder.exists()) {
			if (!dataFolder.mkdirs()) {
				return false;
			}
		}

		armorStandFile = new File(dataFolder, "armorstands.asc");
		if (!armorStandFile.exists()) {
			try {
				if (!armorStandFile.createNewFile()) {
					return false;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}

		animationFile = new File(dataFolder, "animations.asc");
		if (!animationFile.exists()) {
			try {
				if (!animationFile.createNewFile()) {
					return false;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}

		savesFolder = new File(pluginFolder, "saves");
		if (!savesFolder.exists()) {
			if (!savesFolder.mkdirs()) {
				return false;
			}
		}

		return true;
	}

	public FileResult saveSingleArmorStand(ArmorStandWrapper wrapper, String name, Location origin) {
		String identifier = wrapper.getIdentifier();
		if (name == null) {
			name = identifier;
		}

		File targetFile = new File(savesFolder, name + ".ascs");

		if (targetFile.exists()) {
			return new FileResult(FileResult.Result.INVALID_FILE, targetFile);
		}

		JSONObject json = new JSONObject();
		json.put("stand", wrapper.toJSON(origin));
		json.put("type", SaveType.SINGLE.name());

		boolean written = writeJSON(json, targetFile);

		if (!written) {
			return new FileResult(FileResult.Result.FAILURE, targetFile);
		}

		return new FileResult(FileResult.Result.SUCCESS, targetFile);
	}

	public FileResult saveMultipleArmorStands(List<ArmorStandWrapper> wrappers, String name, Player player, boolean saveBlocks) {
		Location origin = player.getLocation();
		if (wrappers == null || wrappers.isEmpty()) {
			throw new IllegalArgumentException("Cannot save empty list of wrappers");
		}
		String identifier = wrappers.get(0).getIdentifier();
		if (name == null) {
			name = identifier;
		}

		File targetFile = new File(savesFolder, name + ".ascs");

		if (targetFile.exists()) {
			return new FileResult(FileResult.Result.INVALID_FILE, targetFile);
		}

		JSONArray wrapperArray = new JSONArray();
		for (ArmorStandWrapper wrapper : wrappers) {
			wrapperArray.put(wrapper.toJSON(origin));
		}

		JSONObject json = new JSONObject();
		json.put("stands", wrapperArray);
		json.put("type", SaveType.MULTI.name());
		if (saveBlocks && ConfigManager.Entry.SAVE_SELECTION_BLOCKS.get(false)) {
			if (!ArmorStandCustomizer.useWorldEdit()) {
				plugin.getLogger().warning("Saving blocks is enabled, but WorldEdit is not installed");
			} else {
				WorldEditManager worldEditManager = ArmorStandCustomizer.getInstance().getWorldEditManager();
				json.put("selection", worldEditManager.serializeSelection(player));
			}
		}

		boolean written = writeJSON(json, targetFile);

		if (!written) {
			return new FileResult(FileResult.Result.FAILURE, targetFile);
		}

		return new FileResult(FileResult.Result.SUCCESS, targetFile);
	}

	public FileResult<?> loadArmorStands(String name, Session editor, Player player, boolean loadBlocks, boolean ignoreAir) {
		Location origin = player.getLocation();
		File targetFile = new File(savesFolder, name + ".ascanim");
		if (!targetFile.exists()) {
			targetFile = new File(savesFolder, name + ".ascs");
		}

		if (!targetFile.exists()) {
			return new FileResult<>(FileResult.Result.INVALID_FILE, targetFile);
		}

		JSONObject json = readJSONObject(targetFile);

		if (json == null) {
			return new FileResult<>(FileResult.Result.FAILURE, targetFile);
		}

		SaveType type = SaveType.valueOf(json.getString("type"));

		if (type == SaveType.SINGLE) {
			return loadSingleArmorStand(json, targetFile, editor, origin);
		}
		if (type == SaveType.MULTI) {
			return loadMultipleArmorStands(json, targetFile, editor, player, loadBlocks, ignoreAir);
		}
		if (type == SaveType.ANIMATION) {
			return loadAnimation(origin, name, null);
		}
		throw new IllegalArgumentException("Could not load ArmorStand data of type " + type);
	}

	protected FileResult<ArmorStandWrapper> loadSingleArmorStand(JSONObject json, File targetFile, Session editor, Location origin) {
		ArmorStandWrapper wrapper = ArmorStandWrapper.load(json.getJSONObject("stand"), editor, origin);

		return new FileResult<>(FileResult.Result.SUCCESS, targetFile, wrapper);
	}

	protected FileResult<List<ArmorStandWrapper>> loadMultipleArmorStands(JSONObject json, File targetFile, Session editor, Player player, boolean loadBlocks, boolean ignoreAir) {
		Location origin = player.getLocation();
		JSONArray wrapperArray = json.getJSONArray("stands");

		List<ArmorStandWrapper> wrapperList = new ArrayList<>();

		for (int i = 0; i < wrapperArray.length(); i++) {
			ArmorStandWrapper wrapper = ArmorStandWrapper.load(wrapperArray.getJSONObject(i), editor, origin);
			wrapperList.add(wrapper);
		}

		if (json.has("selection")) {
			if (loadBlocks && ConfigManager.Entry.LOAD_SELECTION_BLOCKS.get(false)) {
				if (ArmorStandCustomizer.useWorldEdit()) {
					WorldEditManager worldEditManager = ArmorStandCustomizer.getInstance().getWorldEditManager();
					worldEditManager.deserializeSelection(player, json.getJSONObject("selection"), ignoreAir);
				}
			}
		}

		return new FileResult<>(FileResult.Result.SUCCESS, targetFile, wrapperList);
	}

	public FileResult saveAnimation(Animation animation, String name, Location origin) {
		File targetFile = new File(savesFolder, name + ".ascanim");

		if (targetFile.exists()) {
			return new FileResult(FileResult.Result.INVALID_FILE, targetFile);
		}

		JSONObject json = animation.toJSON(origin);
		json.put("type", SaveType.ANIMATION.name());

		boolean written = writeJSON(json, targetFile);

		if (!written) {
			return new FileResult(FileResult.Result.FAILURE, targetFile);
		}

		return new FileResult(FileResult.Result.SUCCESS, targetFile);
	}

	public FileResult<Animation> loadAnimation(Location origin, String name, String newName) {
		File targetFile = new File(savesFolder, name + ".ascanim");

		if (!targetFile.exists()) {
			return new FileResult<>(FileResult.Result.INVALID_FILE, targetFile);
		}

		JSONObject json = readJSONObject(targetFile);

		AnimationManager animationManager = ArmorStandCustomizer.getInstance().getAnimationManager();
		Animation animation = animationManager.loadAnimation(origin, json, newName != null ? newName : name);

		return new FileResult<>(FileResult.Result.SUCCESS, targetFile, animation);
	}

	public boolean saveArmorStands() {
		plugin.getLogger().info("Saving ArmorStands...");

		JSONArray armorStands = armorStandManager.registrationsToJSON();
		return writeJSON(armorStands, armorStandFile);
	}

	public boolean loadArmorStands() {
		plugin.getLogger().info(" ");
		plugin.getLogger().info("Loading ArmorStands...");

		JSONArray array = readJSONArray(armorStandFile);
		armorStandManager.loadRegistrations(array);
		return true;
	}

	public boolean saveAnimations() {
		plugin.getLogger().info("Saving Animations....");

		JSONArray animations = animationManager.registrationsToJSON();
		return writeJSON(animations, animationFile);
	}

	public boolean loadAnimations() {
		plugin.getLogger().info(" ");
		plugin.getLogger().info("Loading Animations...");

		JSONArray array = readJSONArray(animationFile);
		animationManager.loadRegistrations(array);
		return true;
	}

	// Upload / Download

	public void uploadFile(final String fileName, final Player uploader, final Callback<String> callback) {
		List<File> files = findFile(savesFolder, fileName);
		try {
			if (files.size() == 1) {
				File file = files.get(0);

				if (file == null) { throw new FileNotFoundException(); }
				String type = file.getName().substring(fileName.length() + 1);// File extension
				String data = readJSONObject(file).toString();

				uploadData(fileName, type, data, uploader, callback);
			} else {
				if (files.isEmpty()) {
					throw new FileNotFoundException();
				}
				throw new IllegalArgumentException("MULTIPLE_FILES");
			}
		} catch (Throwable throwable) {
			callback.call(null, throwable);
		}
	}

	public void downloadFile(final String id, final Callback<FileResult> callback) {
		try {
			final Callback<String> innerCallback = new Callback<String>() {
				@Override
				public void call(String value, Throwable error) {
					if (error != null) {
						callback.call(null, error);
					}

					try {
						JSONObject json = new JSONObject(value);

						String name = json.getString("name");
						String type = json.getString("type");

						JSONObject data = json.getJSONObject("data");

						File targetFile = new File(savesFolder, String.format("%s-%s.%s", name, id, type)); // name-id.type

						if (targetFile.exists()) {
							callback.call(new FileResult(FileResult.Result.INVALID_FILE, targetFile), null);
							return;
						}

						boolean written = writeJSON(data, targetFile);

						if (!written) {
							callback.call(new FileResult(FileResult.Result.FAILURE, targetFile), null);
							return;
						}

						callback.call(new FileResult(FileResult.Result.SUCCESS, targetFile), null);
					} catch (Throwable throwable) {
						callback.call(null, throwable);
					}
				}
			};
			downloadData(id, innerCallback);
		} catch (Throwable throwable) {
			callback.call(null, throwable);
		}
	}

	public void uploadData(final String name, final String type, final String data, final Player uploader, final Callback<String> callback) {
		if (name == null || type == null || data == null || callback == null) { throw new IllegalArgumentException(); }
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					final URL url = new URL("https://asc.inventivetalent.org/upload/");

					//Write the data
					ByteArrayOutputStream bout = new ByteArrayOutputStream();
					final PrintStream print = new PrintStream(bout);
					print.print(data);

					//Open the connection
					HttpURLConnection connection = (HttpURLConnection) url.openConnection();
					connection.setRequestProperty("User-Agent", "ArmorStandCustomizer/" + ArmorStandCustomizer.getInstance().getDescription().getVersion());
					connection.setDoInput(true);
					connection.setDoOutput(true);
					connection.setRequestMethod("POST");
					connection.setInstanceFollowRedirects(false);

					final boolean includePlayer = ConfigManager.Entry.SNOOPER_PLAYER.get(false);
					final boolean includeServer = ConfigManager.Entry.SNOOPER_SERVER.get(false);
					String serverAddress = ConfigManager.Entry.SNOOPER_SERVER_ADDRESS.get("");
					if (serverAddress.isEmpty()) {
						serverAddress = Bukkit.getServer().getIp();
					}
					final String address = serverAddress;

					//Build metadata
					JSONObject meta = new JSONObject();
					meta.put("name", name);
					meta.put("type", type);
					meta.put("uploader", new JSONObject() {
						{
							if (includePlayer) {
								put("player", new JSONObject() {{
									put("name", uploader.getName());
									put("uuid", uploader.getUniqueId().toString());
								}});
							} else {
								plugin.getLogger().warning("snooper.player.upload is disabled. The uploader will not be displayed properly online.");
							}
							if (includeServer) {
								put("server", new JSONObject() {{
									put("name", Bukkit.getServer().getServerName());
									put("online_mode", Bukkit.getOnlineMode());
									put("address", address);
									put("host", new JSONObject() {
										ArmorStandCustomizer instance = ArmorStandCustomizer.getInstance();

										{
											put("name", instance.getHostName());
											put("address", instance.getHostAddress());
										}
									});
								}});
							}
						}
					});

					//Write the data to the connection
					OutputStream out = connection.getOutputStream();
					out.write(String.format("meta=%s&data=", meta.toString()).getBytes("UTF-8"));
					out.write(bout.toString("UTF-8").getBytes("UTF-8"));
					out.flush();
					out.close();

					//Read the response
					BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

					String content = "";
					String line = null;
					while ((line = reader.readLine()) != null) {
						content += line;
					}

					connection.getInputStream().close();

					callback.call(content, null);
				} catch (Throwable e) {
					callback.call(null, e);
				}
			}
		}).start();
	}

	public void downloadData(final String id, final Callback<String> callback) throws MalformedURLException {
		if (id == null || callback == null) { throw new IllegalArgumentException(); }

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					final URL url = new URL(String.format("https://asc.inventivetalent.org/download/?id=%s", id));

					//Open the connection
					URLConnection connection = url.openConnection();
					connection.setRequestProperty("User-Agent", "ArmorStandCustomizer/" + ArmorStandCustomizer.getInstance().getDescription().getVersion());

					//Read the response
					BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

					String content = "";
					String line = null;
					while ((line = reader.readLine()) != null) {
						content += line;
					}
					connection.getInputStream().close();

					callback.call(content, null);
				} catch (Throwable e) {
					callback.call(null, e);
				}
			}
		}).start();
	}

	//Write

	private boolean writeJSON(JSONObject json, File file) {
		return writeString(json.toString(ConfigManager.Entry.FILES_COMPACT.get(false) ? 0 : 2), file);
	}

	private boolean writeJSON(JSONArray json, File file) {
		return writeString(json.toString(ConfigManager.Entry.FILES_COMPACT.get(false) ? 0 : 2), file);
	}

	private boolean writeString(String string, File file) {
		timings.SAVE.startTiming();
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(file));

			out.write(string);
			out.flush();
			out.close();

			timings.SAVE.stopTiming();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		timings.SAVE.stopTiming();
		return false;
	}

	//Read

	private JSONObject readJSONObject(File file) {
		String string = readString(file);

		if (string == null) {
			return null;
		}

		if (string.length() < 2) {
			return new JSONObject();
		}

		return new JSONObject(string);
	}

	private JSONArray readJSONArray(File file) {
		String string = readString(file);

		if (string == null) {
			return null;
		}

		if (string.length() < 2) {
			return new JSONArray();
		}

		return new JSONArray(string);
	}

	private String readString(File file) {
		timings.LOAD.startTiming();
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));

			String content = "";
			String line = null;
			while ((line = in.readLine()) != null) {
				content += line;
			}
			in.close();

			timings.LOAD.stopTiming();
			return content;
		} catch (IOException e) {
			e.printStackTrace();
		}
		timings.LOAD.stopTiming();
		return null;
	}

	//Util

	private List<File> findFile(File baseFolder, String name) {
		if (baseFolder == null || name == null) { throw new IllegalArgumentException(); }
		if (!baseFolder.isDirectory()) {
			throw new IllegalArgumentException("File is not a directory");
		}
		File[] files = baseFolder.listFiles();
		if (files == null) { return null; }

		List<File> matchingFiles = new ArrayList<>();

		for (File file : files) {
			String stripped = file.getName();
			if (stripped.toLowerCase().equals(name.toLowerCase())) {
				return Arrays.asList(file);
			}
			int dotIndex = stripped.lastIndexOf('.');
			if (dotIndex == -1) { continue; }
			stripped = stripped.substring(0, dotIndex);
			if (stripped.toLowerCase().equals(name.toLowerCase())) {
				matchingFiles.add(file);
			}
		}
		return matchingFiles;
	}

}
