/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.session;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.task.TaskManager;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SessionManager {

	private ArmorStandCustomizer plugin;

	private final Map<UUID, Session> sessions = new HashMap<>();

	public SessionManager(ArmorStandCustomizer plugin) {
		this.plugin = plugin;
	}

	public Session createSession(Player player) {
		if (player == null) { throw new IllegalArgumentException("player cannot be null"); }
		if (getSession(player) != null) {
			throw new IllegalStateException(player.getName() + " already has a session");
		}
		Session session = new Session(player.getUniqueId());
		sessions.put(player.getUniqueId(), session);

		TaskManager taskManager = ArmorStandCustomizer.getInstance().getTaskManager();
		if (taskManager.isRunning) {
			taskManager.axisParticles.add(session);
			taskManager.pathParticles.add(session);
		}
		return session;
	}

	public void removeSession(OfflinePlayer player) {
		if (player == null) { throw new IllegalArgumentException("player cannot be null"); }
		if (getSession(player) == null) {
			throw new IllegalStateException(player.getName() + " has no session");
		}
		sessions.remove(player.getUniqueId());
	}

	public Session getSession(OfflinePlayer player) {
		if (player == null) { throw new IllegalArgumentException("player cannot be null"); }
		return sessions.get(player.getUniqueId());
	}

	public boolean hasSession(OfflinePlayer player) {
		return sessions.containsKey(player.getUniqueId());
	}

	public Session getArmorStandSession(ArmorStand stand) {
		if (stand == null) { return null; }
		for (Session session : getSessions()) {
			if (stand.equals(session.getEditing())) {
				return session;
			}
		}
		return null;
	}

	public Collection<Session> getSessions() {
		return sessions.values();
	}

}
