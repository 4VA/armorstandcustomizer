/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.session;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.animator.Animator;
import org.inventivetalent.asc.config.ConfigManager;
import org.inventivetalent.asc.editor.Editor;
import org.inventivetalent.asc.editor.InputMode;
import org.inventivetalent.asc.editor.RotationMode;
import org.inventivetalent.asc.stand.ArmorStandPart;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.undo.UndoTracker;
import org.inventivetalent.asc.undo.Undoable;
import org.inventivetalent.chat.ChatAPI;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Session {

	private final double MAX_STEP_MOVE     = 0.5D;
	private final double MAX_STEP_ROTATION = 45D;

	private final double MIN_STEP_MOVE     = 0.03125D;
	private final double MIN_STEP_ROTATION = 0.5D;

	protected final UUID player;

	protected ArmorStandWrapper editing;
	protected InputMode inputMode = InputMode.SCROLL;
	protected Editor editor;
	protected ArmorStandPart editingPart  = ArmorStandPart.RIGHT_ARM;
	protected RotationMode   rotationMode = RotationMode.BODY;
	protected Animator animator;

	protected List<String> selection = new ArrayList<>();

	protected UndoTracker undoTracker = new UndoTracker();

	protected double moveSteps     = MAX_STEP_MOVE;
	protected double rotationSteps = MAX_STEP_ROTATION;

	protected boolean particles = true;

	protected List<String> chatBuffer = new CopyOnWriteArrayList<>();

	public Session(@Nonnull UUID player) {
		this.player = player;
	}

	public OfflinePlayer getPlayer() {
		Player player = Bukkit.getPlayer(this.player);
		if (player != null) {
			return player;
		}
		return Bukkit.getOfflinePlayer(this.player);
	}

	public void setEditing(ArmorStandWrapper editing) {
		if (editing == null || editing != this.editing) {
			if (this.editing != null) {
				this.editing.setEditor(null);
			}
		}
		this.editing = editing;
		if (editor == null && editing != null) {
			editor = new Editor(this);
		}
		if (this.editing != null) {
			this.editing.setEditor(this);
			fillInventory();
		}
	}

	public ArmorStandWrapper getEditing() {
		return editing;
	}

	public boolean isEditing() {
		return getEditing() != null;
	}

	public void stopEditing(boolean removeSession) {
		if (getPlayer().isOnline()) {
			if (isEditing()) {
				sendMessage(getPlayer().getPlayer(), "§aSaved §7\"%s\"", getEditing().getIdentifier());
			}
		}
		setEditing(null);
		clearInventory();

		clearSelection();

		if (removeSession) {
			if (getPlayer().isOnline()) {
				sendMessage(getPlayer().getPlayer(), "§aYou are no longer editing.");
			}
			ArmorStandCustomizer.getInstance().getSessionManager().removeSession(getPlayer());

			if (ConfigManager.Entry.LOG_CHAT.get(false) && !chatBuffer.isEmpty() && getPlayer().isOnline()) {
				sendMessage("§a ˅ Start of chat log ˅ ");
				getPlayer().getPlayer().sendMessage(" ");

				for (String chat : chatBuffer) {
					sendRawMessage(chat);
				}

				getPlayer().getPlayer().sendMessage(" ");
				sendMessage("§c ˄ End of chat log ˄ ");
				sendMessage("§7You missed %s messages while editing.", chatBuffer.size());
				chatBuffer.clear();
			}
		}
	}

	public InputMode getInputMode() {
		return inputMode;
	}

	public void setInputMode(InputMode inputMode) {
		this.inputMode = inputMode;

		sendMessage("§aChanged input mode to §7%s", inputMode.name());
	}

	public void setEditingPart(ArmorStandPart part) {
		if (part == null) {
			throw new IllegalArgumentException("part cannot be null");
		}
		this.editingPart = part;

		sendMessage("§aYou are now editing the §2%s§a.", part.toPlainText());
		updateInfoMessage("§aNow editing §7%s§a", getEditingPart().toPlainText());
	}

	public ArmorStandPart getEditingPart() {
		return editingPart;
	}

	public void switchRotationMode() {
		if (getRotationMode() == RotationMode.PART) {
			setRotationMode(RotationMode.BODY);
		} else if (getRotationMode() == RotationMode.BODY) {
			setRotationMode(RotationMode.PART);
		}

		sendMessage("§bChanged rotation mode to §9%s", getRotationMode().name().toLowerCase());
		updateInfoMessage("§bRotation mode: §9%s", getRotationMode().name().toLowerCase());
	}

	public void setRotationMode(RotationMode rotationMode) {
		this.rotationMode = rotationMode;
	}

	public RotationMode getRotationMode() {
		return rotationMode;
	}

	public void setEditor(Editor editor) {
		this.editor = editor;
	}

	public Editor getEditor() {
		return editor;
	}

	public double getMoveSteps() {
		return moveSteps;
	}

	public void setMoveSteps(double moveSteps) {
		this.moveSteps = moveSteps;
		sendMessage("§bMovement steps: §9%s§b", moveSteps);
	}

	public double getRotationSteps() {
		return rotationSteps;
	}

	public void setRotationSteps(double rotationSteps) {
		this.rotationSteps = rotationSteps;
		sendMessage("§bRotation steps: §9%s§b", rotationSteps);
	}

	public void cycleSteps(boolean reversed) {
		double prevMove = getMoveSteps();
		double prevRot = getRotationSteps();

		double newMove = reversed ? (prevMove * 2.0D) : (prevMove / 2.0D);
		double newRot = reversed ? (prevRot * 2.0D) : (prevRot / 2.0D);

		if (newMove < MIN_STEP_MOVE) {
			sendMessage("§cYou reached the minimum movement steps");
		} else if (newMove > MAX_STEP_MOVE) {
			sendMessage("§cYou reached the maximum movement steps");
		} else {
			setMoveSteps(newMove);
		}

		if (newRot < MIN_STEP_ROTATION) {
			sendMessage("§cYou reached the minimum rotation steps");
		} else if (newRot > MAX_STEP_ROTATION) {
			sendMessage("§cYou reached the maximum rotation steps");
		} else {
			setRotationSteps(newRot);
		}

		updateInfoMessage("§bMovement: §9%s§b, Rotation: §9%s§b", getMoveSteps(), getRotationSteps());
	}

	public boolean isInSelection(String identifier) {
		return selection.contains(identifier);
	}

	public void updateSelection(Collection<ArmorStandWrapper> wrappers) {
		if (selection.isEmpty()) {
			for (ArmorStandWrapper wrapper : wrappers) {
				selection.add(wrapper.getIdentifier());
			}
		}
	}

	public void addToSelection(String identifier) {
		selection.add(identifier);
	}

	public void removeFromSelection(String identifier) {
		selection.remove(identifier);
	}

	public void clearSelection() {
		selection.clear();
	}

	public void trackUndo(Undoable undoable) {
		undoTracker.track(undoable);
	}

	public void undo(int amount) {
		int size = undoTracker.undoable();
		if (undoTracker.undo(amount)) {
			sendMessage("§aUndo successful (%s)", amount);
		} else {
			sendMessage("§cNothing left to undo (%s/%s)", size, amount);
		}
	}

	public void redo(int amount) {
		int size = undoTracker.redoable();
		if (undoTracker.redo(amount)) {
			sendMessage("§aRedo successful (%s)", amount);
		} else {
			sendMessage("§cNothing left to redo (%s/%s)", size, amount);
		}
	}

	public Animator getAnimator() {
		return animator;
	}

	public void setAnimator(Animator animator) {
		this.animator = animator;
	}

	public boolean isAnimating() {
		return getAnimator() != null && getAnimator().isAnimating();
	}

	public boolean hasParticles() {
		return particles;
	}

	public void setParticles(boolean particles) {
		this.particles = particles;
	}

	public void logChat(String chat) {
		if (ConfigManager.Entry.LOG_CHAT.get(false)) {
			chatBuffer.add(chat);
		}
	}

	public void sendMessage(String message, Object... format) {
		if (getPlayer().isOnline()) {
			sendMessage(getPlayer().getPlayer(), message, format);
		}
	}

	public void sendMessage(CommandSender receiver, String message, Object... format) {
		try {
			message = String.format(message, format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		receiver.sendMessage(ArmorStandCustomizer.getPrefix() + message);
	}

	public void sendRawMessage(String message, Object... format) {
		if (getPlayer().isOnline()) {
			sendRawMessage(getPlayer().getPlayer(), message, format);
		}
	}

	public void sendRawMessage(CommandSender receiver, String message, Object... format) {
		if (!(receiver instanceof Player)) {
			throw new IllegalArgumentException("receiver must be a player");
		}
		try {
			message = String.format(message, format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ChatAPI.sendRawMessage((Player) receiver, message);
	}

	public void updateInfoMessage(String message, Object... format) {
		if (!getPlayer().isOnline()) {
			return;
		}
		message = String.format(message, format);

		ChatAPI.sendRawMessage(getPlayer().getPlayer(), "{\"text\":\"" + message + "\"}", ChatAPI.ACTION_BAR);
	}

	private void fillInventory() {
		if (!getPlayer().isOnline()) {
			return;
		}
		Player player = getPlayer().getPlayer();
		Inventory inventory = player.getInventory();

		for (int i = 0; i < 9; i++) {
			ItemStack item = inventory.getItem(i);
			if (item == null || item.getType() == Material.AIR) {
				inventory.setItem(i, HOTBAR_PLACEHOLDER);
			}
		}
	}

	private void clearInventory() {
		if (!getPlayer().isOnline()) {
			return;
		}
		Player player = getPlayer().getPlayer();
		Inventory inventory = player.getInventory();

		for (int i = 0; i < 9; i++) {
			ItemStack item = inventory.getItem(i);
			if (HOTBAR_PLACEHOLDER.isSimilar(item)) {
				inventory.setItem(i, null);
			}
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		Session session = (Session) o;

		if (Double.compare(session.moveSteps, moveSteps) != 0) { return false; }
		if (Double.compare(session.rotationSteps, rotationSteps) != 0) { return false; }
		if (editing != null ? !editing.equals(session.editing) : session.editing != null) { return false; }
		if (editingPart != session.editingPart) { return false; }
		if (editor != null ? !editor.equals(session.editor) : session.editor != null) { return false; }
		if (player != null ? !player.equals(session.player) : session.player != null) { return false; }

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = player != null ? player.hashCode() : 0;
		result = 31 * result + (editing != null ? editing.hashCode() : 0);
		result = 31 * result + (editor != null ? editor.hashCode() : 0);
		result = 31 * result + (editingPart != null ? editingPart.hashCode() : 0);
		temp = Double.doubleToLongBits(moveSteps);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(rotationSteps);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	public static final ItemStack HOTBAR_PLACEHOLDER = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData());

	static {
		ItemMeta meta = HOTBAR_PLACEHOLDER.getItemMeta();
		meta.setDisplayName("§8");
		meta.setLore(Arrays.asList("Hi, I'm a placeholder.", "Nice to meet you! :)", "I am here for the drop-function to work properly,", "but you can replace me with any other item"));
		HOTBAR_PLACEHOLDER.setItemMeta(meta);
	}
}

