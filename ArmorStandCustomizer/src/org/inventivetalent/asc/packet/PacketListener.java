/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.packet;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.config.ConfigManager;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.session.SessionManager;
import org.inventivetalent.packetlistener.handler.PacketHandler;
import org.inventivetalent.packetlistener.handler.PacketOptions;
import org.inventivetalent.packetlistener.handler.ReceivedPacket;
import org.inventivetalent.packetlistener.handler.SentPacket;
import org.inventivetalent.reflection.resolver.minecraft.NMSClassResolver;
import org.json.JSONObject;

public class PacketListener extends PacketHandler {

	public PacketListener(Plugin plugin) {
		super(plugin);
	}

	@Override
	@PacketOptions(forcePlayer = true)
	public void onSend(SentPacket packet) {
		final Player player = packet.getPlayer();

		SessionManager sessionManager = ArmorStandCustomizer.getInstance().getSessionManager();

		Session session = sessionManager.getSession(player);
		if (session != null) {
			if (session.isEditing() || session.isAnimating()) {
				if (packet.getPacketName().equals("PacketPlayOutChat")) {
					if (ConfigManager.Entry.EDITOR_DISABLE_CHAT_IN.get(false)) {
						Object a = packet.getPacketValue("a");
						byte b = (byte) packet.getPacketValue("b");
						if (b != 1) { return; }
						try {
							String raw = (String) ChatSerializer.getDeclaredMethod("a", IChatBaseComponent).invoke(null, a);
							if (isEmpty(raw)) {
								return;
							}

							JSONObject json = new JSONObject(raw);
							if (isEmpty(json.getString("text")) && (!json.has("extra") || isEmpty(json.getJSONArray("extra").getString(0)))) {
								return;
							}

							//Allow messages with the ASC prefix
							if (!raw.contains("{\"color\":\"aqua\",\"text\":\"[\"},{\"color\":\"dark_green\",\"text\":\"AS\"},{\"color\":\"gold\",\"text\":\"C\"},{\"color\":\"aqua\",\"text\":\"]\"}") && !raw.contains(ArmorStandCustomizer.getPrefix())) {
								packet.setCancelled(true);
								session.logChat(raw);
							}
						} catch (Exception e) {
						}
					}
				}
			}
		}
	}

	@Override
	@PacketOptions(forcePlayer = true)
	public void onReceive(ReceivedPacket packet) {
		final Player player = packet.getPlayer();

		SessionManager sessionManager = ArmorStandCustomizer.getInstance().getSessionManager();

		Session session = sessionManager.getSession(player);
		if (session != null) {
			if (session.isEditing() || session.isAnimating()) {
				if (packet.getPacketName().equals("PacketPlayInChat")) {
					String a = (String) packet.getPacketValue("a");
					if (!a.startsWith("/")) {
						if (ConfigManager.Entry.EDITOR_DISABLE_CHAT_OUT.get(false)) {
							packet.setCancelled(true);
							sendMessage(player, "§cYou can't send chat messages while editing.");
						}
					}
				}
			}
		}
	}

	public void sendMessage(CommandSender receiver, String message, Object... format) {
		try {
			message = String.format(message, format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		receiver.sendMessage(ArmorStandCustomizer.getPrefix() + message);
	}

	public boolean isEmpty(String string) {
		if (string.isEmpty()) { return true; }
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) != ' ') { return false; }
		}
		return true;
	}

	static NMSClassResolver nmsClassResolver = new NMSClassResolver();

	private static Class<?> IChatBaseComponent = nmsClassResolver.resolveSilent("IChatBaseComponent");
	private static Class<?> ChatSerializer     = nmsClassResolver.resolveSilent("ChatSerializer", "IChatBaeComponent$ChatSerailizer");

}
