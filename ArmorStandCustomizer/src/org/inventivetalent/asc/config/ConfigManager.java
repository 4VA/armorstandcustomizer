/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.config;

import org.bukkit.plugin.java.JavaPlugin;
import org.inventivetalent.asc.ArmorStandCustomizer;

public class ConfigManager {

	private JavaPlugin plugin;

	private int VERSION = 1;

	public ConfigManager(JavaPlugin plugin) {
		this.plugin = plugin;
		plugin.saveDefaultConfig();

		int configVersion = get(Entry.VERSION, 0);

		if (configVersion < VERSION) {
			plugin.getLogger().warning(" ");
			plugin.getLogger().warning("           Your configuration seems to be out of date ");
			plugin.getLogger().warning(" Make sure you copy the latest configuration from the project page ");
			plugin.getLogger().warning("           https://www.spigotmc.org/resources/10536/ ");
			plugin.getLogger().warning(" ");
		}
	}

	public <T> T get(Entry entry, T def) {
		String path = entry.getPath();
		return get(path, def);
	}

	public <T> T get(String path, T def) {
		try {
			Object value = plugin.getConfig().get(path);
			return (T) value;
		} catch (Throwable e) {
			plugin.getLogger().warning("Could not cast config value to requested class (" + path + " > " + def.getClass() + "): " + e.getMessage());
		}
		return def;
	}

	public enum Entry {

		VERSION("version"),
		ANIMATOR_SMOOTHER_ACCURACY("animator.smoother.accuracy"),
		EDITOR_ALLOW_MULTIPLE("editor.allow_multiple"),
		EDITOR_DISABLE_CHAT_IN("editor.disable.chat.in"),
		EDITOR_DISABLE_CHAT_OUT("editor.disable.chat.out"),
		SAVE_SELECTION_BLOCKS("save.selection.blocks"),
		LOAD_SELECTION_BLOCKS("load.selection.blocks"),
		LOG_COMMAND_EXCEPTIONS("log.command.exceptions"),
		LOG_CHAT("log.chat"),
		FILES_COMPACT("files.compact"),
		SNOOPER_PLAYER("snooper.player.upload"),
		SNOOPER_SERVER("snooper.server.upload"),
		SNOOPER_SERVER_ADDRESS("snooper.server.address");

		private String path;

		private Entry(String path) {
			this.path = path;
		}

		public String getPath() {
			return path;
		}

		public <T> T get(T def) {
			ConfigManager manager = ArmorStandCustomizer.getInstance().getConfigManager();
			return manager.get(this, def);
		}
	}

}
