/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.info;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.util.MetaHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ArmorStandInfo extends CustomizerCommand {

	public ArmorStandInfo(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		List<Entity> nearbyEntities = player.getNearbyEntities(8, 8, 8);
		List<ArmorStand> armorStands = new ArrayList<>();
		for (Entity ent : nearbyEntities) {
			if (ent instanceof ArmorStand) {
				armorStands.add((ArmorStand) ent);
			}
		}

		ArmorStand lookingAt = null;

		List<Block> blocks = player.getLineOfSight((Set) null, 8);
		for (Block block : blocks) {
			if (block.getType() != Material.AIR) {
				break;
			}

			for (Entity ent : armorStands) {
				Location loc = ent.getLocation();
				Location feet = new Location(ent.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
				Location head = feet.add(0, 1, 0);

				if (block.getLocation().equals(feet) || block.getLocation().equals(head)) {
					lookingAt = (ArmorStand) ent;
					break;
				}
			}
		}

		ArmorStandManager manager = ArmorStandCustomizer.getInstance().getArmorStandManager();

		if (lookingAt != null) {
			ArmorStandWrapper wrapper = manager.isRegistered(lookingAt.getUniqueId()) ? manager.getArmorStand(lookingAt.getWorld(), lookingAt.getUniqueId(), MetaHelper.getMetaValue(lookingAt, "ASC_ID", String.class)) : null;

			if (wrapper != null) {
				sendMessage(sender, "§aYou are looking at §7\"%s\"", wrapper.getIdentifier());
				printRegistration(player, wrapper.getWrapped().getUniqueId(), wrapper.getIdentifier());
				return true;
			} else {
				sendMessage(sender, "§cYou are looking at an ArmorStand, but it is not registered.");
			}
		} else {
			sendMessage(sender, "§cYou are not looking at an ArmorStand");
		}

		List<ArmorStandWrapper> wrappers = new ArrayList<>();
		for (ArmorStand stand : armorStands) {
			ArmorStandWrapper wrapper = manager.isRegistered(stand.getUniqueId()) ? manager.getArmorStand(stand.getWorld(), stand.getUniqueId(), MetaHelper.getMetaValue(stand, "ASC_ID", String.class)) : null;

			if (wrapper != null) {
				wrappers.add(wrapper);
			}
		}

		if (!wrappers.isEmpty()) {
			sendMessage(sender, "§a-- Nearby ArmorStands --");

			for (ArmorStandWrapper wrapper : wrappers) {
				printRegistration(player, wrapper.getWrapped().getUniqueId(), wrapper.getIdentifier());
			}
		}

		return true;
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
	}

	public static ArmorStandInfo init(PluginCommand command) {
		ArmorStandInfo cmd = new ArmorStandInfo(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "info");

		cmd.label = "ascinfo";

		return cmd;
	}
}
