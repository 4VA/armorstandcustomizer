/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.base;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.session.Session;

import java.util.List;

public class ArmorStandCustomizer extends CustomizerCommand {

	private ArmorStandCustomizer(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		sendMessage(sender, "§2ArmorStand§6Customizer §aplugin");
		sendRawMessage(sender, "{\"text\":\"\",\"extra\":[{\"text\":\"%1$s\"},{\"text\":\"Use\",\"color\":\"green\"},{\"text\":\" /aschelp \",\"color\":\"gray\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/aschelp\"}},{\"text\":\"to get started\",\"color\":\"green\"}]}", org.inventivetalent.asc.ArmorStandCustomizer.getPrefix());
		sendRawMessage(sender, "{\"text\":\"\",\"extra\":[{\"text\":\"%1$s\"},{\"text\":\"Or read the Wiki at \",\"color\":\"green\"},{\"text\":\"asc.inventivetalent.org/wiki\",\"color\":\"yellow\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"https://asc.inventivetalent.org/wiki/\"}}]}", org.inventivetalent.asc.ArmorStandCustomizer.getPrefix());

		return true;
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
		if (!command.testPermissionSilent(sender)) { return; }
	}

	public static ArmorStandCustomizer init(PluginCommand command) {
		ArmorStandCustomizer cmd = new ArmorStandCustomizer(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "base");

		cmd.label = "asc";

		return cmd;
	}
}
