/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.selection;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CommandOption;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandAttribute;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.worldedit.SelectionHelper;
import org.inventivetalent.asc.worldedit.WorldEditManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static org.inventivetalent.asc.command.CommandOption.Option.SIMPLE;

@CommandOption(options = { SIMPLE })
public class ArmorStandSelection extends CustomizerCommand {

	public ArmorStandSelection(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		//		if (!validateArgumentLength(sender, args, 1)) {
		//			return false;
		//		}

		WorldEditManager worldEditManager = ArmorStandCustomizer.getInstance().getWorldEditManager();

		if (!worldEditManager.hasSelection(player)) {
			sendMessage(sender, "§cPlease make a WorldEdit selection first");
			return false;
		}

		List<ArmorStandWrapper> wrappers = worldEditManager.getArmorStandsInSelection(player);

		session.updateSelection(wrappers);

		if (wrappers.isEmpty()) {
			sendMessage(sender, "§cSelection is empty");
			return false;
		}

		if (args.length == 0) {
			printSelections(wrappers, player, session);
			return true;
		}

		String action = args[0];

		List<String> identifiers = new ArrayList<>();
		for (ArmorStandWrapper wrapper : wrappers) {
			if (session.isInSelection(wrapper.getIdentifier())) {
				identifiers.add(wrapper.getIdentifier());
			}
		}

		ArmorStandManager armorStandManager = ArmorStandCustomizer.getInstance().getArmorStandManager();

		if ("delete".equalsIgnoreCase(action)) {
			for (String id : identifiers) {
				player.chat(String.format("/ascdelete %s", id));
			}
			session.clearSelection();
		} else if ("attributes".equalsIgnoreCase(action)) {
			String merged = mergeArray(args, 1, " ");
			for (String id : identifiers) {
				player.chat(String.format("/ascedit %s %s", id, merged));
			}
		} else if ("move".equalsIgnoreCase(action)) {
			if (!validateArgumentLength(sender, args, 4)) {
				return false;
			}

			double x = 0;
			double y = 0;
			double z = 0;

			try {
				x = Double.parseDouble(args[1]);
				y = Double.parseDouble(args[2]);
				z = Double.parseDouble(args[3]);
			} catch (Exception e) {
				sendMessage(sender, "§cInvalid number: %s", e.getMessage());
				return false;
			}

			for (String id : identifiers) {
				player.chat(String.format("/ascmove %s %s %s %s", id, x, y, z));
			}
			session.clearSelection();
		} else if ("rotate".equalsIgnoreCase(action)) {
			if (!validateArgumentLength(sender, args, 2)) {
				return false;
			}

			double x = 0;
			double y = 0;
			double z = 0;

			try {
				y = Double.parseDouble(args[1]);
				if (args.length > 2) { x = Double.parseDouble(args[2]); }
				if (args.length > 3) { z = Double.parseDouble(args[3]); }
			} catch (Exception e) {
				sendMessage(sender, "§cInvalid number: %s", e.getMessage());
				return false;
			}

			EulerAngle angle = new EulerAngle(Math.toRadians(x), Math.toRadians(y), Math.toRadians(z));

			Vector base = worldEditManager.getMin(player);

			for (String id : identifiers) {
				ArmorStandWrapper wrapper = armorStandManager.getArmorStand(player.getWorld(), id);

				Vector oldVector = wrapper.getWrapped().getLocation().toVector();
				oldVector = oldVector.subtract(base);

				Vector newVector = SelectionHelper.rotateVector(oldVector, angle);
				newVector = newVector.add(base);

				wrapper.rotateRelative((float) angle.getX());

				wrapper.teleport(newVector.toLocation(player.getWorld()));
			}

		} else if ("flip".equalsIgnoreCase(action)) {
			boolean simple = false;
			CommandOption.Option[] options = parseCommandOptions(sender, args);
			if (options == null) { return false; }
			for (CommandOption.Option option : options) {
				if (option == SIMPLE) {
					simple = true;
				}
			}

			Vector axis = player.getLocation().getDirection();
			if (simple) {
				axis.setX(90 * ((int) ((axis.getX() + 45.0) / 90.0)));
				axis.setY(90 * ((int) ((axis.getY() + 45.0) / 90.0)));
				axis.setZ(90 * ((int) ((axis.getZ() + 45.0) / 90.0)));
			}

			Vector base = worldEditManager.getMin(player);

			for (String id : identifiers) {
				ArmorStandWrapper wrapper = armorStandManager.getArmorStand(player.getWorld(), id);

				Vector oldVector = wrapper.getWrapped().getLocation().toVector();
				oldVector = oldVector.subtract(base);

				Vector newVector = SelectionHelper.flipVector(oldVector, axis);
				newVector = newVector.add(base);

				wrapper.rotateRelative(180f);

				wrapper.teleport(newVector.toLocation(player.getWorld()));
			}

		} else if ("click_sel".equals(action)) {
			session.addToSelection(args[1]);
			printSelections(wrappers, player, session);
		} else if ("click_desel".equals(action)) {
			session.removeFromSelection(args[1]);
			printSelections(wrappers, player, session);
		} else {
			sendMessage(sender, "§cUnknown action: §7%s", action);
			return false;
		}

		return true;
	}

	public void printSelections(Collection<ArmorStandWrapper> wrappers, Player player, Session session) {
		player.sendMessage("  ");

		for (ArmorStandWrapper wrapper : wrappers) {
			printSelectionLine(player, session, wrapper.getIdentifier(), wrapper.getWrapped().getUniqueId());
		}
	}

	public void printSelectionLine(Player receiver, Session session, String identifier, UUID id) {
		String prefix = "{\"text\":\"%1$s\"},";

		String separator1 = "{\"text\":\" [\",\"color\":\"gray\"},";
		String separator2 = "{\"text\":\"] \",\"color\":\"gray\"},";

		String selected = "{\"text\":\"✔\",\"color\":\"green\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascselection click_desel %2$s\"}},";
		String deselected = "{\"text\":\"✖\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascselection click_sel %2$s\"},\"bold\":\"false\"},";

		String entity = "{\"text\":\"%2$s\",\"color\":\"gray\",\"hoverEvent\":{\"action\":\"show_entity\",\"value\":\"{id:%3$s,name:%2$s,type:ArmorStand}\"}},";
		String separator = "{\"text\":\" | \",\"color\":\"dark_gray\",\"bold\":\"true\"},";
		String teleport = "{\"text\":\"[Teleport] \",\"color\":\"green\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascteleport %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to teleport to %2$s\",\"color\":\"gray\"}]}}},";
		String edit = "{\"text\":\"[Edit] \",\"color\":\"gold\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascedit %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to edit %2$s\",\"color\":\"gray\"}]}}},";
		String delete = "{\"text\":\"[Delete] \",\"color\":\"red\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/ascdelete %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to delete this ArmorStand\",\"color\":\"gray\"}]}}}";
		String extras = prefix + separator1 + (session.isInSelection(identifier) ? selected : deselected) + separator2 + entity + separator + teleport + edit + delete;

		sendRawMessage(receiver, "{\"text\":\"\",\"extra\":[" + extras + "]}", ArmorStandCustomizer.getPrefix(), identifier, id);
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
		if (!command.testPermissionSilent(sender)) {
			return;
		}

		if (args.length == 1) {
			list.add("delete");
			list.add("attributes");
			list.add("move");
		}
		if (args.length >= 2) {
			if ("attributes".equalsIgnoreCase(args[0])) {
				for (ArmorStandAttribute attribute : ArmorStandAttribute.values()) {
					list.add(attribute.name().toLowerCase());
				}
			}
			if ("flip".equalsIgnoreCase(args[1])) {
				for (CommandOption.Option option : getCommandOptions()) {
					list.add("-" + option.getKey());
				}
			}
		}
	}

	public static ArmorStandSelection init(PluginCommand command) {
		ArmorStandSelection cmd = new ArmorStandSelection(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "selection");

		cmd.label = "ascselection";

		return cmd;
	}
}
