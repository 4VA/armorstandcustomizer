/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.animate;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.animator.*;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.command.exception.InvalidSessionException;
import org.inventivetalent.asc.data.DataManager;
import org.inventivetalent.asc.data.FileResult;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.task.Callback;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ArmorStandAnimate extends CustomizerCommand {

	public ArmorStandAnimate(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(final CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			throw new InvalidSessionException();
		}

		if (!validateArgumentLength(sender, args, 1)) {
			return false;
		}

		AnimationManager animationManager = ArmorStandCustomizer.getInstance().getAnimationManager();

		if ("create".equalsIgnoreCase(args[0])) {
			if (session.isAnimating()) {
				sendMessage(sender, "§cYou are already animating");
				sendMessage(sender, "§cPlease stop animating with §7/ascanimate finish§c first");
				return false;
			}

			if (args.length < 2) {
				sendMessage(sender, "§cPlease specify an animation name");
				return false;
			}

			String name = args[1];

			if (animationManager.isRegistered(name)) {
				sendMessage(sender, "§cAnimation §7\"%s\"§c already exists.", name);
				return false;
			}

			Animator animator = new Animator(session);
			animator.setAnimation(animationManager.createAnimation(name));

			session.setAnimator(animator);

			sendMessage(sender, "§aYou can now start animating");
			animator.sendChatInterface();
			return true;
		}

		if ("remove".equalsIgnoreCase(args[0])) {
			if (args.length < 2) {
				sendMessage(sender, "§cPlease specify an animation name");
				return false;
			}

			String name = args[1];

			if (!animationManager.isRegistered(name)) {
				sendMessage(sender, "§cAnimation §7\"%s\" §cis not registered.", name);
				return false;
			}

			animationManager.unregister(name);

			sendMessage(sender, "§aRemoved Animation §7\"%s\"", name);
			return true;
		}

		if ("edit".equalsIgnoreCase(args[0])) {
			if (session.isAnimating()) {
				sendMessage(sender, "§cYou are already animating");
				sendMessage(sender, "§cPlease  stop animating with §7/ascanimate finish§c first");
				return false;
			}

			if (args.length < 2) {
				sendMessage(sender, "§cPlease specify an animation name");
				return false;
			}

			String name = args[1];

			if (!animationManager.isRegistered(name)) {
				sendMessage(sender, "§cAnimation §7\"%s\"§c does not exist", name);
				return false;
			}

			Animator animator = new Animator(session);
			Animation animation = animationManager.getAnimation(name);

			animator.setAnimation(animation);

			session.setAnimator(animator);

			sendMessage(sender, "§aYou are now editing §7\"%s\"", name);

			animator.sendChatInterface();
			return true;
		}

		if ("load".equalsIgnoreCase(args[0])) {
			if (args.length < 2) {
				sendMessage(sender, "§cPlease specify the animation name");
				return false;
			}

			if (session.isAnimating()) {
				sendMessage(sender, "§cYou are already animating");
				return false;
			}

			String name = args[1];
			String newName = args.length > 2 ? args[2] : null;

			if (((newName == null || newName.equals(name)) && animationManager.isRegistered(name)) || (newName != null && animationManager.isRegistered(newName))) {
				sendMessage(sender, "§cAnimation is already registered");
				return false;
			}

			FileResult result = animationManager.loadAnimation(player, name, newName);

			switch (result.getResult()) {
				case SUCCESS:
					sendMessage(sender, "§aLoaded Animation from §7\"%s\"§a as §7%s.", result.getRelativeFile(), (newName != null ? newName : name));
					break;
				case INVALID_FILE:
					sendMessage(sender, "§cFile §7\"%s\"§c does not exist", result.getRelativeFile());
					break;
				case FAILURE:
					sendMessage(sender, "§cFailed to load file. See console for details");
					break;
			}

			return true;
		}

		if ("list".equalsIgnoreCase(args[0])) {
			Collection<String> names = animationManager.getAnimationNames();
			if (names.isEmpty()) {
				sendMessage(sender, "§cNo Animations found");
			} else {
				sendMessage(sender, "§a-- Registered Animations --");

				for (String name : names) {
					printAnimation(player, name);
				}
			}
			return true;
		}

		Animator animator = null;
		if ((animator = session.getAnimator()) == null || !animator.isAnimating()) {
			sendMessage(sender, "§cYou are not animating yet");
			sendMessage(sender, "§cStart animating with §7/ascanimate create§c or §7/ascanimate edit");
			return false;
		}

		if ("finish".equalsIgnoreCase(args[0])) {
			animator.setAnimation(null);
			session.setAnimator(null);

			sendMessage(sender, "§cYou are no longer animating");
			return true;
		}

		final Animation animation = animator.getAnimation();

		//Play/Stop
		if ("play".equalsIgnoreCase(args[0])) {
			if (animation.isRunning()) {
				sendMessage(sender, "§cAnimation is already playing");
				return false;
			}
			animator.saveCurrentState();
			animationManager.playAnimation(animation);

			sendMessage(sender, "§aNow playing animation (Interval: §7%s§a)", animation.getInterval());
			return true;
		}
		if ("stop".equalsIgnoreCase(args[0])) {
			if (!animation.isRunning()) {
				sendMessage(sender, "§cAnimation is not playing");
				return false;
			}
			animationManager.stopAnimation(animation);

			sendMessage(sender, "§aStopped animation");
			return true;
		}

		//Animation settings
		if ("settings".equalsIgnoreCase(args[0])) {
			if (!validateArgumentLength(sender, args, 2)) { return false; }
			if ("autoplay".equalsIgnoreCase(args[1])) {
				if (!validateArgumentLength(sender, args, 3)) { return false; }
				boolean autoplay = parseArgument(args, 2, false);
				animation.setAutoplay(autoplay);
				sendMessage(sender, "§aAutoplay changed to §7%s", autoplay);
				return true;
			}
		}

		//Interval control
		if ("interval".equalsIgnoreCase(args[0])) {
			if (args.length > 1) {
				if ("increase".equalsIgnoreCase(args[1])) {
					animation.setInterval(animation.getInterval() + 1);
				} else if ("decrease".equalsIgnoreCase(args[1])) {
					if (animation.getInterval() > 1) {
						animation.setInterval(animation.getInterval() - 1);
					}
				} else {
					try {
						animation.setInterval(Integer.parseInt(args[1]));
					} catch (Exception e) {
					}
				}
				animator.sendChatInterface();
				return true;
			}
		}

		//Save / Load
		if ("save".equals(args[0])) {
			FileResult result = null;
			if (args.length == 1) {
				result = animationManager.saveAnimation(animation, player.getLocation());
			} else {
				DataManager dataManager = ArmorStandCustomizer.getInstance().getDataManager();
				result = dataManager.saveAnimation(animationManager.getAnimation(animation), args[1], player.getLocation());
			}

			String relativeFile = result.getRelativeFile();

			switch (result.getResult()) {
				case SUCCESS:
					sendMessage(sender, "§aSaved Animation to §7\"%s\"§a.", relativeFile);
					break;
				case INVALID_FILE:
					sendMessage(sender, "§cThe file §7\"%s\"§c already exists.", relativeFile);
					sendMessage(sender, "§cPlease specify a different name.");
					break;
				case FAILURE:
					sendMessage(sender, "§cFailed to save file. See console for details.");
					break;
			}
			return true;
		}

		//Frame control
		if ("firstFrame".equalsIgnoreCase(args[0])) {
			JSONObject state = session.isEditing() ? session.getEditing().toJSON() : null;
			if (animation.setCurrentFrame(0)) { animator.saveCurrentState(state); }
			animator.sendChatInterface();
			return true;
		}
		if ("prevFrame".equalsIgnoreCase(args[0])) {
			if (animation.getCurrentFrame() > 0) {
				JSONObject state = session.isEditing() ? session.getEditing().toJSON() : null;
				if (animation.setCurrentFrame(animation.getCurrentFrame() - 1)) { animator.saveCurrentState(state); }
			}
			animator.sendChatInterface();
			return true;
		}
		if ("nextFrame".equalsIgnoreCase(args[0])) {
			if (animation.getCurrentFrame() < animation.getLength() - 1) {
				JSONObject state = session.isEditing() ? session.getEditing().toJSON() : null;
				if (animation.setCurrentFrame(animation.getCurrentFrame() + 1)) { animator.saveCurrentState(state); }
			}
			animator.sendChatInterface();
			return true;
		}
		if ("lastFrame".equalsIgnoreCase(args[0])) {
			JSONObject state = session.isEditing() ? session.getEditing().toJSON() : null;
			if (animation.setCurrentFrame(animation.getLength() - 1)) { animator.saveCurrentState(state); }
			animator.sendChatInterface();
			return true;
		}

		//Frame add/remove
		if ("removeframe".equalsIgnoreCase(args[0])) {
			if (animation.getLength() > 0) {
				animation.removeCurrentFrame();
				animator.sendChatInterface();
				return true;
			}
		}

		ArmorStandWrapper wrapper = null;
		if ((wrapper = session.getEditing()) == null) {
			sendMessage(sender, "§cYou are not editing an ArmorStand you could animate");
			return false;
		}

		if ("addframe".equalsIgnoreCase(args[0])) {
			animator.saveCurrentState();
			animation.addFrame(wrapper);
			animator.sendChatInterface();
			return true;
		}
		if ("insertFrame".equalsIgnoreCase(args[0])) {
			animator.saveCurrentState();
			animation.insertFrame(animation.getCurrentFrame(), wrapper);
			animator.sendChatInterface();
			return true;
		}
		if ("resetframe".equalsIgnoreCase(args[0])) {
			animator.loadCurrentState();
			return true;
		}

		if ("smooth".equalsIgnoreCase(args[0])) {
			if (!validateArgumentLength(sender, args, 2)) {
				return false;
			}

			if ("all".equalsIgnoreCase(args[1])) {
				if (animation.getLength() <= 1) {
					sendMessage(sender, "§cCan't smooth single-frame animation");
					return false;
				}

				final Callback<Void> callback = new Callback<Void>() {
					@Override
					public void call(Void value, Throwable error) {
						sendMessage(sender, "§aDone!");
					}
				};
				Bukkit.getScheduler().runTaskAsynchronously(ArmorStandCustomizer.getInstance(), new Runnable() {
					@Override
					public void run() {
						sendMessage(sender, "§aSmoothing all frames...");
						AnimationSmoother.smoothAll(animation, callback);
					}
				});

				return true;
			}

			if (!validateArgumentLength(sender, args, 3)) {
				return false;
			}

			final int startFrame = Integer.parseInt(args[1]);
			final int endFrame = Integer.parseInt(args[2]);

			if (animation.getLength() < startFrame || animation.getLength() < endFrame) {
				sendMessage(sender, "§cInvalid frame position");
				return false;
			}

			final Callback<List<Frame>> callback = new Callback<List<Frame>>() {
				@Override
				public void call(List<Frame> value, Throwable t) {
					sendMessage(sender, "§aDone! Applying changes...");

					animation.insertFrame(startFrame, value);

					sendMessage(sender, "§aInserted %s Frames", value.size());
				}
			};

			sendMessage(sender, "§aSmoothing animation...");

			Bukkit.getScheduler().runTaskAsynchronously(ArmorStandCustomizer.getInstance(), new Runnable() {
				@Override
				public void run() {
					AnimationSmoother smoother = new AnimationSmoother(animation.getFrame(startFrame), animation.getFrame(endFrame));

					smoother.setCallback(callback);

					smoother.perform();
				}
			});

			return true;
		}

		sendMessage(sender, "§cInvalid argument: %s", args[0]);

		return false;
	}

	protected void printAnimation(Player receiver, String identifier) {
		String prefix = "{\"text\":\"%1$s\"},";
		String name = "{\"text\":\"%2$s\",\"color\":\"gray\"},";
		String separator = "{\"text\":\" | \",\"color\":\"dark_gray\",\"bold\":\"true\"},";
		String edit = "{\"text\":\"[Edit] \",\"color\":\"gold\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate edit %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to edit %2$s\",\"color\":\"gray\"}]}}},";
		String delete = "{\"text\":\"[Delete] \",\"color\":\"red\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/ascanimate remove %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to delete this Animation\",\"color\":\"gray\"}]}}}";
		String extras = prefix + name + separator + edit + delete;

		sendRawMessage(receiver, "{\"text\":\"\",\"extra\":[" + extras + "]}", ArmorStandCustomizer.getPrefix(), identifier);
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
		if (!command.testPermissionSilent(sender)) {
			return;
		}

		if (args.length == 1) {
			list.addAll(Arrays.asList("create", "remove", "edit", "load", "save", "play", "stop", "finish", "settings", "smooth", "list"));
		}
		if (args.length == 2) {
			if ("edit".equalsIgnoreCase(args[0])) {
				AnimationManager manager = ArmorStandCustomizer.getInstance().getAnimationManager();
				list.addAll(manager.getAnimationNames());
			}
			if ("settings".equalsIgnoreCase(args[0])) {
				list.addAll(Arrays.asList("autoplay"));
			}
		}
		if (args.length == 3) {
			if ("settings".equalsIgnoreCase(args[0])) {
				if ("autoplay".equalsIgnoreCase(args[1])) {
					list.add("true");
					list.add("false");
				}
			}
		}
	}

	@Override
	public String getUsage(String usage, CommandSender sender, String[] args) {
		if (args.length == 0) {
			usage += "<create | edit | list | load | save | play | stop | finish | smooth | settings> ";
		}
		if (args.length == 1) {
			if ("settings".equalsIgnoreCase(args[0])) {
				usage += "[autoplay] ";
			}
			if ("smooth".equalsIgnoreCase(args[0])) {
				usage += "[start | all] [end] ";
			}
			if ("edit".equalsIgnoreCase(args[0])) {
				usage += "<animation> ";
			}
		}
		if (args.length == 2) {
			if ("settings".equalsIgnoreCase(args[0])) {
				if ("autoplay".equals(args[1])) {
					usage += "<true | false> ";
				}
			}
		}
		return usage;
	}

	public static ArmorStandAnimate init(PluginCommand command) {
		ArmorStandAnimate cmd = new ArmorStandAnimate(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "animate");

		cmd.label = "ascanimate";

		return cmd;
	}

}
