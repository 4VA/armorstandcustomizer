/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.slot;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandPart;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.chat.ChatAPI;
import org.inventivetalent.reflection.minecraft.Minecraft;

import java.util.List;

public class ArmorStandSlot extends CustomizerCommand {

	public ArmorStandSlot(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		ArmorStandWrapper editing = null;
		if ((editing = session.getEditing()) == null) {
			sendMessage(sender, "§cYou are not editing at the moment.");
			sendMessage(sender, "§cUse §7/ascedit §cto start editing.");
			return false;
		}

		if (!validateArgumentLength(sender, args, 1, true)) {
			sendModel(session, player);
			return false;
		}

		ArmorStandPart part = parseArgument(args, 0, ArmorStandPart.NOT_SET);
		if (part == ArmorStandPart.NOT_SET) {
			sendMessage(sender, "§cUnknown part: §7%s", args[0]);
			return false;
		}

		if (part == ArmorStandPart.LEFT_ARM) {
			if (Minecraft.VERSION.olderThan(Minecraft.Version.v1_9_R1)) {
				sendMessage(sender, "§cLEFT_ARM is only available in 1.9");
				return false;
			}
		}

		session.setEditingPart(part);

		if (part.hasItem()) {
			Material material = parseArgument(args, 1, Material.SOIL);
			Short damage = parseArgument(args, 2, (short) 0);

			ItemStack itemStack = null;
			if (material != Material.SOIL) {
				itemStack = new ItemStack(material, 1, damage);
			} else if (player.getItemInHand() != null && !Session.HOTBAR_PLACEHOLDER.isSimilar(player.getItemInHand())) {
				itemStack = player.getItemInHand().clone();
			}
			if (itemStack != null) { editing.setItem(part, itemStack); }
		} else if (args.length > 1) {
			sendMessage(sender, "§cThe §7%s §cslot cannot hold an item", part);
		}

		sendModel(session, player);

		return true;
	}

	public void sendModel(Session session, Player player) {
		String[] lines = buildModel(session.getEditing(), session.getEditingPart());
		sendMessage(player, " ");
		for (String s : lines) {
			ChatAPI.sendRawMessage(player, s);
		}
	}

	public String[] buildModel(ArmorStandWrapper wrapper, ArmorStandPart selected) {
		String headLine = buildLine(wrapper, selected, "     |||     ", ArmorStandPart.HEAD);

		String[] bodyLines = new String[] {
				buildLine(wrapper, selected, " | -", ArmorStandPart.RIGHT_ARM) + buildLine(wrapper, selected, "-|-", ArmorStandPart.BODY) + buildLine(wrapper, selected, "- |", ArmorStandPart.LEFT_ARM),
				buildLine(wrapper, selected, " |  ", ArmorStandPart.RIGHT_ARM) + buildLine(wrapper, selected, "  |  ", ArmorStandPart.BODY) + buildLine(wrapper, selected, "  |", ArmorStandPart.LEFT_ARM),
				buildLine(wrapper, selected, " |  ", ArmorStandPart.RIGHT_ARM) + buildLine(wrapper, selected, "  |  ", ArmorStandPart.BODY) + buildLine(wrapper, selected, "  |", ArmorStandPart.LEFT_ARM) };

		String[] legLines = new String[] {
				buildLine(wrapper, selected, "   |-", ArmorStandPart.RIGHT_LEG) + buildLine(wrapper, selected, "|", ArmorStandPart.BODY) + buildLine(wrapper, selected, "-|  ", ArmorStandPart.LEFT_LEG),
				buildLine(wrapper, selected, "   | ", ArmorStandPart.RIGHT_LEG) + buildLine(wrapper, selected, "§0-", ArmorStandPart.LEFT_LEG) + buildLine(wrapper, selected, " |  ", ArmorStandPart.LEFT_LEG),
				buildLine(wrapper, selected, "   | ", ArmorStandPart.FEET) + buildLine(wrapper, selected, "§0-", ArmorStandPart.FEET) + buildLine(wrapper, selected, " |  ", ArmorStandPart.FEET) };

		String format = "{\"text\":\"\",\"extra\":[{\"text\":\"%1$s\"}%2$s]}";

		return new String[] {
				String.format(format, ArmorStandCustomizer.getPrefix(), headLine),
				String.format(format, ArmorStandCustomizer.getPrefix(), bodyLines[0]),
				String.format(format, ArmorStandCustomizer.getPrefix(), bodyLines[1]),
				String.format(format, ArmorStandCustomizer.getPrefix(), bodyLines[2]),
				String.format(format, ArmorStandCustomizer.getPrefix(), legLines[0]),
				String.format(format, ArmorStandCustomizer.getPrefix(), legLines[1]),
				String.format(format, ArmorStandCustomizer.getPrefix(), legLines[2]) };
	}

	private String buildLine(ArmorStandWrapper wrapper, ArmorStandPart selected, String content, ArmorStandPart part) {
		String lineFormat = ",{\"text\":\"%1$s\",\"color\":\"%2$s\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascslot %3$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"%4$s\"}]}}}";
		return String.format(lineFormat, content, (selected == part ? "green" : "dark_aqua"), part.name(), (selected == part ? "§eCurrently selected" : "§aClick to select"), ArmorStandCustomizer.getPrefix());
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
		if (!command.testPermissionSilent(sender)) {
			return;
		}

		if (args.length == 1) {
			for (ArmorStandPart part : ArmorStandPart.values()) {
				list.add(part.name());
			}
		}
		if (args.length == 2) {
			for (Material material : Material.values()) {
				list.add(material.name());
			}
		}
	}

	public static ArmorStandSlot init(PluginCommand command) {
		ArmorStandSlot cmd = new ArmorStandSlot(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "edit.slot");

		cmd.label = "ascslot";

		return cmd;
	}

}
