/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.inventivetalent.asc.ArmorStandCustomizer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CommandRegistry {

	private static Map<String, CustomizerCommand> commandMap = new HashMap<>();

	public static void register(String name, Class<? extends CustomizerCommand> clazz) {
		try {
			CustomizerCommand command = (CustomizerCommand) clazz.getDeclaredMethod("init", PluginCommand.class).invoke(null, Bukkit.getPluginCommand(name));
			commandMap.put(name, command);
		} catch (Exception e) {
			ArmorStandCustomizer.getInstance().getLogger().severe("Exception while registering command \"" + name + "\" for class " + clazz.toString());
			throw new RuntimeException(e);
		}
	}

	public static Set<String> getNames() {
		return commandMap.keySet();
	}

	public static CustomizerCommand getCommand(String name) {
		return commandMap.get(name);
	}

	public static String getLabel(String name) {
		return getCommand(name).label;
	}

	public static String getUsage(String name) {
		return getCommand(name).usage.replace("<command>", getLabel(name));
	}

	public static String getDescription(String name) {
		return getCommand(name).description;
	}

	public static String getPermission(String name) {
		return getCommand(name).command.getPermission();
	}

	public static List<String> getAliases(String name) {
		return getCommand(name).command.getAliases();
	}

	public static CommandOption.Option[] getCommandOptions(String name) {
		return getCommand(name).getCommandOptions();
	}

}
