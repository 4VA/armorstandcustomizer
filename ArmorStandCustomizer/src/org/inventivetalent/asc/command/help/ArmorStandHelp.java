/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.help;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CommandOption;
import org.inventivetalent.asc.command.CommandRegistry;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.session.Session;

import java.util.List;

public class ArmorStandHelp extends CustomizerCommand {

	public ArmorStandHelp(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		if (args.length == 0) {

			//Command list
			sendMessage(sender, " ");
			sendMessage(sender, "§aList of commands");
			sendMessage(sender, "§aClick on a command for more information");
			sendMessage(sender, " ");

			String commandFormat = "{\"text\":\"\",\"extra\":[{\"text\":\"%1$s \"},{\"text\":\"/%2$s \",\"color\":\"gray\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/aschelp %3$s \"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here for more information about the \",\"color\":\"gray\"},{\"text\":\"%3$s\",\"color\":\"gold\"},{\"text\":\" command\",\"color\":\"gray\"}]}}},{\"text\":\"| \",\"color\":\"dark_gray\",\"bold\":\"true\"},{\"text\":\"[%4$s]\",\"color\":\"yellow\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Required permission\",\"color\":\"gray\"}]}},\"bold\":\"false\"}]}";

			for (String name : CommandRegistry.getNames()) {
				String label = CommandRegistry.getLabel(name);
				String space = "";
				for (int i = 0; i < (16 - label.length()); i++) {
					space += " ";
				}
				sendRawMessage(sender, commandFormat, ArmorStandCustomizer.getPrefix(), label + space, name, CommandRegistry.getPermission(name));
			}

			return true;
		}

		String name = args[0].toLowerCase();
		if (!CommandRegistry.getNames().contains(name)) {
			sendMessage(sender, "§cCommand §7%s§c not found", name);
			return false;
		}

		sendMessage(sender, " ");

		String aliases = "";
		for (String alias : CommandRegistry.getAliases(name)) {
			aliases += alias + ", ";
		}
		aliases = aliases.substring(0, aliases.length() - 2);

		sendMessage(sender, "§aCommand: §7%s (%s)", name, aliases);
		sendRawMessage(sender, "{\"text\":\"\",\"extra\":[{\"text\":\"%1$s\"},{\"text\":\"Usage: \",\"color\":\"green\"},{\"text\":\"%2$s\",\"color\":\"aqua\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/%3$s\"}}]}", ArmorStandCustomizer.getPrefix(), CommandRegistry.getUsage(name), CommandRegistry.getLabel(name));
		sendMessage(sender, "§aPermission: §e%s", CommandRegistry.getPermission(name));
		sendMessage(sender, "§aDescription: ");
		sendMessage(sender, "§7%s", CommandRegistry.getDescription(name));

		CommandOption.Option[] options = CommandRegistry.getCommandOptions(name);
		if (options.length > 0) {
			sendMessage(sender, "§aOptions: ");
			for (CommandOption.Option option : options) {
				sendMessage(sender, "§e-%s", option.getKey());
			}
		}

		return true;
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {

	}

	public static ArmorStandHelp init(PluginCommand command) {
		ArmorStandHelp cmd = new ArmorStandHelp(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "help");

		cmd.label = "aschelp";

		return cmd;
	}
}
