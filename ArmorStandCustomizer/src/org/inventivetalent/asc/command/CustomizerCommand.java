/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command;

import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.exception.CommandException;
import org.inventivetalent.asc.command.exception.*;
import org.inventivetalent.asc.config.ConfigManager;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.session.SessionManager;
import org.inventivetalent.chat.ChatAPI;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public abstract class CustomizerCommand implements CommandExecutor, TabCompleter {

	protected static String PERM_BASE = "asc.command.";

	protected PluginCommand command;
	protected String        usage;
	protected String label = "asc";
	protected String description;

	public CustomizerCommand(PluginCommand command) {
		this.command = command;
		this.usage = command.getUsage();
		this.description = command.getDescription();
		command.setUsage("");
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
		try {
			return onCommand(commandSender, strings);
		} catch (CommandException e) {
			if (ConfigManager.Entry.LOG_COMMAND_EXCEPTIONS.get(false)) {
				ArmorStandCustomizer.getInstance().getLogger().warning(String.format("%s command performed by %s caused exception", label, commandSender.getName()));
				e.printStackTrace();
			}
			return false;
		} catch (Throwable e) {
			ArmorStandCustomizer.getInstance().getLogger().log(Level.SEVERE, "Exception while calling onCommand for \"" + command.getName() + "\"", e);
			sendMessage(commandSender, "§cAn internal exception occurred while performing this command");
			sendMessage(commandSender, "§cSee console for details");
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
		try {
			List<String> completions = new ArrayList<>();
			getCompletions(completions, sender, command, s, args);
			if (!completions.isEmpty()) {
				return TabCompletionHelper.getPossibleCompletionsForGivenArgs(args, completions.toArray(new String[completions.size()]));
			}
		} catch (CommandException e) {
			if (ConfigManager.Entry.LOG_COMMAND_EXCEPTIONS.get(false)) {
				ArmorStandCustomizer.getInstance().getLogger().warning(String.format("%s tab-complete performed by %s caused exception", label, sender.getName()));
				e.printStackTrace();
			}
		} catch (Throwable e) {
			ArmorStandCustomizer.getInstance().getLogger().log(Level.SEVERE, "Exception while getting tab-completions for \"" + command.getName() + "\"", e);
			sendMessage(sender, "§cAn internal exception occurred while tab-completing this command");
			sendMessage(sender, "§cSee console for details");
		}
		return null;
	}

	private String getUsage0(CommandSender sender, String[] args) {
		String usage = "/<command> ";
		for (String arg : args) {
			usage += arg + " ";
		}
		return getUsage(usage, sender, args);
	}

	public abstract boolean onCommand(CommandSender sender, String[] args);

	public abstract void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args);

	public String getUsage(String usage, CommandSender sender, String[] args) {
		return this.usage;
	}

	protected Session validateSession(Player player) {
		if (!validatePermission(player, command.getPermission())) {
			return null;
		}
		SessionManager manager = ArmorStandCustomizer.getInstance().getSessionManager();
		Session session = null;
		if ((session = manager.getSession(player)) == null) {
			session = manager.createSession(player);
		}
		return session;
	}

	protected boolean validatePermission(Permissible who, String permission) {
		if (!who.hasPermission(permission)) {
			if (who instanceof CommandSender) {
				sendMessage((CommandSender) who, "§cYou don't have enough permissions (%s)", permission);
			}
			throw new MissingPermissionException();
		}
		return true;
	}

	public void sendMessage(CommandSender receiver, String message, Object... format) {
		try {
			message = String.format(message, format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		receiver.sendMessage(ArmorStandCustomizer.getPrefix() + message);
	}

	public void sendRawMessage(CommandSender receiver, String message, Object... format) {
		if (!(receiver instanceof Player)) {
			throw new IllegalArgumentException("receiver must be a player");
		}
		try {
			message = String.format(message, format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ChatAPI.sendRawMessage((Player) receiver, message);
	}

	public boolean validateArgumentLength(CommandSender sender, String[] args, int required) {
		return validateArgumentLength(sender, args, required, false);
	}

	public boolean validateArgumentLength(CommandSender sender, String[] args, int required, boolean allow) {
		if (args.length < required) {
			sendMessage(sender, "§cMissing argument");
			sendUsage(sender, args);
			if (!allow) { throw new InvalidArgumentLengthException(); } else { return false; }
		}
		return true;
	}

	public void sendUsage(CommandSender sender, String[] args) {
		sendUsage(sender, this.label, args);
	}

	public void sendUsage(CommandSender sender, String label, String[] args) {
		sendMessage(sender, ChatColor.GRAY + getUsage0(sender, args).replace("<command>", label));
	}

	protected void printRegistration(Player receiver, UUID id, String identifier) {
		String prefix = "{\"text\":\"%1$s\"},";
		String entity = "{\"text\":\"%2$s\",\"color\":\"gray\",\"hoverEvent\":{\"action\":\"show_entity\",\"value\":\"{id:%3$s,name:%2$s,type:ArmorStand}\"}},";
		String separator = "{\"text\":\" | \",\"color\":\"dark_gray\",\"bold\":\"true\"},";
		String teleport = "{\"text\":\"[Teleport] \",\"color\":\"green\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascteleport %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to teleport to %2$s\",\"color\":\"gray\"}]}}},";
		String edit = "{\"text\":\"[Edit] \",\"color\":\"gold\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascedit %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to edit %2$s\",\"color\":\"gray\"}]}}},";
		String delete = "{\"text\":\"[Delete] \",\"color\":\"red\",\"bold\":\"true\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/ascdelete %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to delete this ArmorStand\",\"color\":\"gray\"}]}}}";
		String extras = prefix + entity + separator + teleport + edit + delete;

		sendRawMessage(receiver, "{\"text\":\"\",\"extra\":[" + extras + "]}", ArmorStandCustomizer.getPrefix(), identifier, id);
	}

	public String mergeArray(String[] array, int offset, String separator) {
		String string = "";

		for (int i = offset; i < array.length; i++) {
			string += array[i] + separator;
		}

		string = string.substring(0, string.length() - separator.length());
		return string;
	}

	public CommandOption.Option[] getCommandOptions() {
		CommandOption optionAnnotation = getClass().getAnnotation(CommandOption.class);
		if (optionAnnotation == null) {
			return new CommandOption.Option[0];
		}
		return optionAnnotation.options();
	}

	@Nullable
	public CommandOption.Option[] parseCommandOptions(CommandSender sender, String[] args) {
		CommandOption.Option[] validOptions = getCommandOptions();

		if (args.length == 0) {
			return new CommandOption.Option[0];
		}

		List<CommandOption.Option> parsed = new ArrayList<>();

		for (int i = args.length - 1; i >= 0; i--) {
			String current = args[i];
			if (current.startsWith("-")) {
				current = current.substring(1);

				CommandOption.Option option = CommandOption.Option.getForKey(current);
				if (option == null) {
					sendMessage(sender, "§cUnknown option: §7%s", current);
					throw new UnknownOptionException();
				}

				if (!ArrayContains(validOptions, option)) {
					sendMessage(sender, "§cThe option §7%s §cis not applicable for this command.", current);
					throw new IncompatibleOptionException();
				}

				parsed.add(option);
			}
		}
		return parsed.toArray(new CommandOption.Option[parsed.size()]);
	}

	public <T> boolean ArrayContains(final T[] array, final T value) {
		for (T t : array) {
			if (t.equals(value)) {
				return true;
			}
		}
		return false;
	}

	public <T> T parseArgument(String[] args, int index, T def) {
		if (def == null) {
			return null;
		}
		T result = (T) parseArgument(args, index, def.getClass());
		if (result == null) {
			return def;
		}
		return result;
	}

	public <T> T parseArgument(String[] args, int index, Class<T> def) {
		if (index >= args.length) {
			return null;
		}
		String arg = args[index];
		if (def != null && String.class.isAssignableFrom(def)) {
			try {
				return (T) arg;
			} catch (Exception e) {
			}
		}

		if (def != null && Enum.class.isAssignableFrom(def)) {
			try {
				return (T) Enum.valueOf((Class<? extends Enum>) def, arg.trim().toUpperCase());
			} catch (Exception e) {
			}
		}

		if (def != null) {
			try {
				Method method = def.getDeclaredMethod("valueOf", String.class);
				return (T) method.invoke(null, arg);
			} catch (Exception e) {
			}
		}

		System.err.println("Could not parse argument '" + arg + "' to any valid object");

		return null;
	}

}

