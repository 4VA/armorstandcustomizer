/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.publish;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.data.DataManager;
import org.inventivetalent.asc.data.FileResult;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.task.Callback;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.List;

public class ArmorStandPublish extends CustomizerCommand {

	public ArmorStandPublish(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(final CommandSender sender, final String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		final Player player = (Player) sender;
		final Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		if (!validateArgumentLength(sender, args, 2)) {
			return false;
		}

		final DataManager dataManager = ArmorStandCustomizer.getInstance().getDataManager();

		final String name = args[1];

		if ("upload".equalsIgnoreCase(args[0])) {
			final Callback<String> callback = new Callback<String>() {
				@Override
				public void call(String value, Throwable error) {
					if (error != null) {
						if (error instanceof FileNotFoundException) {
							sendMessage(sender, "§cFile §7\"%s\"§c not found.", name);
							return;
						}
						if (error instanceof IllegalArgumentException) {
							if (error.getMessage().equals("MULTIPLE_FILES")) {
								sendMessage(sender, "§cMultiple files found.");
								sendMessage(sender, "§cPlease specify the exact file name.");
								return;
							}
						}
						sendMessage(sender, "§cUnexpected error: %s", error.getMessage());
						sendMessage(sender, "§cSee console for details");

						error.printStackTrace();
						return;
					}

					if (value == null) {
						sendMessage(sender, "§cFailed to upload.");
						return;
					}

					String id = null;
					String token = null;

					try {
						JSONObject json = new JSONObject(value);

						if (json.has("error")) {
							sendMessage(sender, "§cServer returned error: §7%s", json.getString("error"));
							return;
						}

						id = json.getString("id");
						token = json.getString("token");
					} catch (JSONException e) {
						sendMessage(sender, "§cReceived invalid response, see console for details.");
						ArmorStandCustomizer.getInstance().getLogger().warning(value);
						e.printStackTrace();
						return;
					}

					sendMessage(sender, "§aUploaded file. You can download it with the following command: ");
					sendRawMessage(sender, String
							.format("{\"text\":\"\",\"extra\":[{\"text\":\"%1$s\"},{\"text\":\"/ascpublish download %2$s\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/ascpublish download %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to copy the command to your chat\",\"color\":\"gray\"}]}}}]}", ArmorStandCustomizer
									.getPrefix(), id));
					sendRawMessage(sender, "{\"text\":\"\",\"extra\":[{\"text\":\"%1$s\"},{\"text\":\"You can also edit it online using the token: \",\"color\":\"green\"},{\"text\":\"%2$s\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"%2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click to copy the token | \",\"color\":\"gray\"},{\"text\":\"Don't lose it, or you won't be able to access the editor\",\"color\":\"red\"}]}}}]}", ArmorStandCustomizer
							.getPrefix(), token);
					sendRawMessage(sender, "{\"text\":\"\",\"extra\":[{\"text\":\"%1$s\"},{\"text\":\"> Click here <\",\"color\":\"gold\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"https://asc.inventivetalent.org/viewer/%2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to visit the website\",\"color\":\"gray\"}]}}}]}", ArmorStandCustomizer
							.getPrefix(), id);
				}
			};

			sendMessage(sender, "§aUploading...");

			Bukkit.getScheduler().runTaskAsynchronously(ArmorStandCustomizer.getInstance(), new Runnable() {
				@Override
				public void run() {
					dataManager.uploadFile(name, player, callback);
				}
			});
			return true;
		}
		if ("download".equalsIgnoreCase(args[0])) {
			final Callback<FileResult> callback = new Callback<FileResult>() {
				@Override
				public void call(FileResult result, Throwable error) {
					if (error != null) {
						if (error instanceof JSONException) {
							sendMessage(sender, "§cThe requested file does not contain valid data.");
							ArmorStandCustomizer.getInstance().getLogger().info("Requested file does not contain valid data: ");
							ArmorStandCustomizer.getInstance().getLogger().info(error.getMessage());
							return;
						}
						sendMessage(sender, "§cUnexpected error: %s", error.getMessage());
						sendMessage(sender, "§cSee console for details");

						error.printStackTrace();
						return;
					}

					String relativeFile = result.getRelativeFile();
					switch (result.getResult()) {
						case SUCCESS:
							sendMessage(sender, "§aDownloaded file to §7\"%s\"§a.", relativeFile);
							sendRawMessage(sender, String
									.format("{\"text\":\"\",\"extra\":[{\"text\":\"%1$s\"},{\"text\":\"/ascload %2$s\",\"color\":\"gray\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascload %2$s\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click here to import the download at your position\",\"color\":\"gray\"}]}}}]}", ArmorStandCustomizer
											.getPrefix(), result.getFileName()));
							break;
						case INVALID_FILE:
							sendMessage(sender, "§cThe file §7\"%s\"§c already exists.", relativeFile);
							break;
						case FAILURE:
							sendMessage(sender, "§cFailed to save file. See console for details.");
							break;
					}
				}
			};

			sendMessage(sender, "§aDownloading...");

			Bukkit.getScheduler().runTaskAsynchronously(ArmorStandCustomizer.getInstance(), new Runnable() {
				@Override
				public void run() {
					dataManager.downloadFile(name, callback);
				}
			});
			return true;
		}

		sendMessage(sender, "§cInvalid argument: §7%s", args[0]);

		return false;
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
		if (!command.testPermissionSilent(sender)) {
			return;
		}

		if (args.length == 1) {
			list.add("download");
			list.add("upload");
		}
	}

	public static ArmorStandPublish init(PluginCommand command) {
		ArmorStandPublish cmd = new ArmorStandPublish(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "publish");

		cmd.label = "ascpublish";

		return cmd;
	}
}
