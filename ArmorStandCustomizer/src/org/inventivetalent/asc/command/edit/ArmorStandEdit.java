/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.edit;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.config.ConfigManager;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandAttribute;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;

import java.util.ArrayList;
import java.util.List;

public class ArmorStandEdit extends CustomizerCommand {

	private ArmorStandEdit(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		if (!validateArgumentLength(sender, args, 1)) {
			return false;
		}

		String identifier = args[0];

		ArmorStandManager armorStandManager = ArmorStandCustomizer.getInstance().getArmorStandManager();

		if (!armorStandManager.isRegistered(identifier)) {
			sendMessage(sender, "§cArmorStand §7\"%s\"§c is not registered", identifier);
			return false;
		}

		ArmorStandWrapper wrapper = null;

		if ((wrapper = armorStandManager.getArmorStand(player.getWorld(), identifier)) == null) {
			sendMessage(sender, "§cCould not find ArmorStand §7\"%s\"§c", identifier);
			return false;
		}

		if (wrapper.getEditor() != null) {
			if (wrapper.getEditor().getPlayer().getUniqueId().equals(session.getPlayer().getUniqueId())) {
				sendMessage(sender, "§cYou are already editing this ArmorStand");
				return false;
			} else if (!allowMultiple()) {
				sendMessage(sender, "§c%s §cis already editing this ArmorStand", wrapper.getEditor().getPlayer().getName());
				return false;
			}
		}

		session.setEditing(wrapper);
		sendMessage(sender, "§aYou are now editing §7\"%s\"§a.", identifier);

		if (args.length >= 2) {
			List<ArmorStandAttribute> attributes = parseAttributes(sender, args, 1);
			wrapper.updateAttributes(attributes);
		}

		return true;
	}

	public boolean allowMultiple() {
		return ConfigManager.Entry.EDITOR_ALLOW_MULTIPLE.get(true);
	}

	public List<ArmorStandAttribute> parseAttributes(CommandSender sender, String[] args, int offset) {
		if (offset > args.length) {
			throw new IllegalArgumentException("offset is bigger than argument length (" + offset + ">" + args.length + ")");
		}
		List<ArmorStandAttribute> attributes = new ArrayList<>();
		for (int i = offset; i < args.length; i++) {
			try {
				ArmorStandAttribute attribute = ArmorStandAttribute.valueOf(args[i].toUpperCase());
				attributes.add(attribute);
			} catch (Exception e) {
				sendMessage(sender, "§cUnknown attribute: §7%s", args[i]);
			}
		}
		return attributes;
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
		if (!command.testPermissionSilent(sender)) {
			return;
		}

		if (args.length == 1) {
			ArmorStandManager manager = ArmorStandCustomizer.getInstance().getArmorStandManager();
			list.addAll(manager.getRegisteredIdentifiers());
		}
		if (args.length >= 2) {
			for (ArmorStandAttribute attribute : ArmorStandAttribute.values()) {
				list.add(attribute.name().toLowerCase());
			}
		}
	}

	public static ArmorStandEdit init(PluginCommand command) {
		ArmorStandEdit cmd = new ArmorStandEdit(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "edit");

		cmd.label = "ascedit";

		return cmd;
	}
}
