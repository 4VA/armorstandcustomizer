/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.save;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CommandOption;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.data.DataManager;
import org.inventivetalent.asc.data.FileResult;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.worldedit.WorldEditManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static org.inventivetalent.asc.command.CommandOption.Option.BLOCK_ORIGIN;
import static org.inventivetalent.asc.command.CommandOption.Option.IGNORE_BLOCKS;

@CommandOption(options = { IGNORE_BLOCKS, BLOCK_ORIGIN })
public class ArmorStandSaveSelection extends CustomizerCommand {

	public ArmorStandSaveSelection(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		if (!validateArgumentLength(sender, args, 1)) {
			return false;
		}

		if (!ArmorStandCustomizer.useWorldEdit()) {
			sendMessage(sender, "§cWorldEdit not found");
			return false;
		}

		WorldEditManager worldEditManager = ArmorStandCustomizer.getInstance().getWorldEditManager();

		if (!worldEditManager.hasSelection(player)) {
			sendMessage(sender, "§cPlease make a WorldEdit selection first");
			return false;
		}

		ArmorStandManager armorStandManager = ArmorStandCustomizer.getInstance().getArmorStandManager();
		DataManager dataManager = ArmorStandCustomizer.getInstance().getDataManager();

		List<ArmorStandWrapper> armorStands = new ArrayList<>();

		Collection<ArmorStand> stands = worldEditManager.getEntitiesInSelection(player, ArmorStand.class);

		for (ArmorStand stand : stands) {
			UUID uuid = stand.getUniqueId();
			if (armorStandManager.isRegistered(uuid)) {
				String identifier = armorStandManager.getIdentifier(uuid);
				ArmorStandWrapper wrapper = armorStandManager.getArmorStand(player.getWorld(), uuid, identifier);

				armorStands.add(wrapper);
			}
		}

		if (armorStands.isEmpty()) {
			sendMessage(sender, "§cNo ArmorStands in your current selection found");
			return false;
		}

		String name = null;
		if (args.length >= 1) {
			name = args[0];
		}

		boolean saveBlocks = true;
		boolean blockOrigin = false;

		CommandOption.Option[] parsedOptions = parseCommandOptions(sender, args);
		if (parsedOptions == null) { return false; }
		for (CommandOption.Option option : parsedOptions) {
			if (option == IGNORE_BLOCKS) {
				saveBlocks = false;
			}
			if (option == BLOCK_ORIGIN) {
				blockOrigin = true;
			}
		}

		if (blockOrigin) {
			Location loc = player.getLocation();
			player.setVelocity(new Vector());
			player.teleport(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
		}

		FileResult result = dataManager.saveMultipleArmorStands(armorStands, name, player, saveBlocks);
		String relativeFile = result.getRelativeFile();

		switch (result.getResult()) {
			case SUCCESS:
				sendMessage(sender, "§aSaved ArmorStands to §7\"%s\"§a.", relativeFile);
				break;
			case INVALID_FILE:
				sendMessage(sender, "§cThe file §7\"%s\"§c already exists.", relativeFile);
				sendMessage(sender, "§cPlease specify a different name.");
				break;
			case FAILURE:
				sendMessage(sender, "§cFailed to save file. See console for details.");
				break;
		}

		return true;
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
		if (args.length >= 2) {
			for (CommandOption.Option option : getCommandOptions()) {
				list.add("-" + option.getKey());
			}
		}
	}

	public static ArmorStandSaveSelection init(PluginCommand command) {
		ArmorStandSaveSelection cmd = new ArmorStandSaveSelection(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "saveselection");

		cmd.label = "ascsaveselection";

		return cmd;
	}
}
