/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.save;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CommandOption;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.data.DataManager;
import org.inventivetalent.asc.data.FileResult;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.stand.ArmorStandWrapper;

import java.util.List;

import static org.inventivetalent.asc.command.CommandOption.Option.BLOCK_ORIGIN;

@CommandOption(options = { BLOCK_ORIGIN })
public class ArmorStandSave extends CustomizerCommand {

	public ArmorStandSave(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		if (!validateArgumentLength(sender, args, 1)) {
			return false;
		}

		String identifier = args[0];

		ArmorStandManager armorStandManager = ArmorStandCustomizer.getInstance().getArmorStandManager();

		if (!armorStandManager.isRegistered(identifier)) {
			sendMessage(sender, "§cArmorStand §7\"%s\"§c is not registered", identifier);
			return false;
		}

		ArmorStandWrapper wrapper = null;

		if ((wrapper = armorStandManager.getArmorStand(player.getWorld(), identifier)) == null) {
			sendMessage(sender, "§cCould not find ArmorStand §7\"%s\"§c", identifier);
			return false;
		}

		DataManager dataManager = ArmorStandCustomizer.getInstance().getDataManager();

		String name = null;
		if (args.length >= 2) {
			name = args[1];
		}

		boolean blockOrigin = false;

		CommandOption.Option[] parsedOptions = parseCommandOptions(sender, args);
		if (parsedOptions == null) { return false; }
		for (CommandOption.Option option : parsedOptions) {
			if (option == BLOCK_ORIGIN) {
				blockOrigin = true;
			}
		}

		if (blockOrigin) {
			Location loc = player.getLocation();
			player.setVelocity(new Vector());
			player.teleport(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
		}

		FileResult result = dataManager.saveSingleArmorStand(wrapper, name, player.getLocation());
		String relativeFile = result.getRelativeFile();

		switch (result.getResult()) {
			case SUCCESS:
				sendMessage(sender, "§aSaved ArmorStand to §7\"%s\"§a.", relativeFile);
				break;
			case INVALID_FILE:
				sendMessage(sender, "§cThe file §7\"%s\"§c already exists.", relativeFile);
				sendMessage(sender, "§cPlease specify a different name.");
				break;
			case FAILURE:
				sendMessage(sender, "§cFailed to save file. See console for details.");
				break;
		}

		return true;
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {
		if (!command.testPermissionSilent(sender)) {
			return;
		}

		if (args.length == 1) {
			ArmorStandManager manager = ArmorStandCustomizer.getInstance().getArmorStandManager();
			list.addAll(manager.getRegisteredIdentifiers());
		}
		if (args.length == 2) {
			String arg = args[0];
			list.add(arg);
		}
		if (args.length >= 3) {
			for (CommandOption.Option option : getCommandOptions()) {
				list.add("-" + option.getKey());
			}
		}
	}

	public static ArmorStandSave init(PluginCommand command) {
		ArmorStandSave cmd = new ArmorStandSave(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "save");

		cmd.label = "ascsave";

		return cmd;
	}

}
