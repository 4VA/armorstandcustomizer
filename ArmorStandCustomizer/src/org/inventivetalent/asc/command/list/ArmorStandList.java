/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.command.list;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.command.CustomizerCommand;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.stand.ArmorStandManager;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ArmorStandList extends CustomizerCommand {

	public ArmorStandList(PluginCommand command) {
		super(command);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) {
			sendMessage(sender, "This command is only available for players");
			return false;
		}
		Player player = (Player) sender;
		Session session = validateSession(player);
		if (session == null) {
			return false;
		}

		ArmorStandManager armorStandManager = ArmorStandCustomizer.getInstance().getArmorStandManager();

		Map<UUID, String> registrations = armorStandManager.getRegistrations();

		if (registrations.isEmpty()) {
			sendMessage(sender, "§cNo registrations found");
			return true;
		}

		int page = Math.max(1, parseArgument(args, 0, 1));
		int pageSize = 8;

		int pageOffset = pageSize * (page - 1);

		int count = 0;
		boolean hasSelection = ArmorStandCustomizer.getInstance().getWorldEditManager().hasSelection(player);

		if (hasSelection) {
			sendMessage(sender, "§a-- ArmorStands in your current selection (Page #%s) --", page);

			Collection<ArmorStand> stands = ArmorStandCustomizer.getInstance().getWorldEditManager().getEntitiesInSelection(player, ArmorStand.class);

			for (ArmorStand stand : stands) {
				UUID uuid = stand.getUniqueId();
				if (armorStandManager.isRegistered(uuid)) {
					String identifier = armorStandManager.getIdentifier(uuid);

					if (count >= pageOffset) { printRegistration(player, uuid, identifier); }
					if (count >= pageOffset + pageSize - 1) { break; }
					count++;
				}
			}
		} else {
			sendMessage(sender, "§a-- All registered ArmorStands (Page #%s) --", page);

			for (Map.Entry<UUID, String> entry : registrations.entrySet()) {
				UUID id = entry.getKey();
				String identifier = entry.getValue();

				if (count >= pageOffset) { printRegistration(player, id, identifier); }
				if (count >= pageOffset + pageSize - 1) { break; }
				count++;
			}
		}

		if (count == 0) {
			sendMessage(sender, "§cNo registrations found");
		}

		return true;
	}

	@Override
	public void getCompletions(List<String> list, CommandSender sender, Command command, String s, String[] args) {

	}

	public static ArmorStandList init(PluginCommand command) {
		ArmorStandList cmd = new ArmorStandList(command);
		command.setExecutor(cmd);
		command.setTabCompleter(cmd);

		command.setPermission(PERM_BASE + "list");

		cmd.label = "asclist";

		return cmd;
	}
}
