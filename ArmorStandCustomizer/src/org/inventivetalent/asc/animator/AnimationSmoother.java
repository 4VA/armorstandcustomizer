/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.animator;

import org.bukkit.Location;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.config.ConfigManager;
import org.inventivetalent.asc.stand.ArmorStandPart;
import org.inventivetalent.asc.task.Callback;
import org.inventivetalent.asc.task.Container;
import org.inventivetalent.asc.util.JSONSerializer;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AnimationSmoother {

	private final Frame firstFrame;
	private final Frame lastFrame;

	private final List<Frame> tempFrames = new ArrayList<>();

	private Callback<List<Frame>> callback;

	public AnimationSmoother(Frame first, Frame last) {
		this.firstFrame = first;
		this.lastFrame = last;
	}

	public static int getAccuracy() {
		return ConfigManager.Entry.ANIMATOR_SMOOTHER_ACCURACY.get(10);
	}

	public void setCallback(Callback<List<Frame>> callback) {
		this.callback = callback;
	}

	public List<Frame> getResult() {
		return tempFrames;
	}

	public void perform() {
		Vector startVector = firstFrame.getLocation().toVector();
		Vector endVector = lastFrame.getLocation().toVector();

		Vector locationStep = new Vector();
		locationStep.setX((endVector.getX() - startVector.getX()) / getAccuracy());
		locationStep.setY((endVector.getY() - startVector.getY()) / getAccuracy());
		locationStep.setZ((endVector.getZ() - startVector.getZ()) / getAccuracy());

		float rotationStep = (lastFrame.getLocation().getYaw() / firstFrame.getLocation().getYaw() / getAccuracy());
		if (lastFrame.getLocation().getYaw() == firstFrame.getLocation().getYaw()) { rotationStep = 0; }

		for (int i = 0; i < getAccuracy(); i++) {
			Location loc = startVector.clone().add(locationStep.clone().multiply(i + 1)).clone().toLocation(firstFrame.getLocation().getWorld());
			loc.setYaw(firstFrame.getLocation().getYaw() + (rotationStep * (i + 1)));

			JSONObject json = firstFrame.getData();
			json.put("location", JSONSerializer.LocationToJSON(loc));
			Frame frame = new Frame(null, json, null);

			for (ArmorStandPart part : ArmorStandPart.values()) {
				EulerAngle startAngle = firstFrame.getPose(part);
				EulerAngle endAngle = lastFrame.getPose(part);

				if (startAngle == null || endAngle == null) { continue; }

				EulerAngle angleStep = new EulerAngle(0, 0, 0);
				angleStep = angleStep.setX((endAngle.getX() - startAngle.getX()) / getAccuracy());
				angleStep = angleStep.setY((endAngle.getY() - startAngle.getY()) / getAccuracy());
				angleStep = angleStep.setZ((endAngle.getZ() - startAngle.getZ()) / getAccuracy());

				EulerAngle angle = startAngle.add(angleStep.getX() * (i + 1), angleStep.getY() * (i + 1), angleStep.getZ() * (i + 1));

				frame.setPose(part, angle);
			}

			// For some reason, loading the data into an Armorstand is required for the frame update to work (I currently have no idea how to solve this differently. TODO: Update)
			frame.getWrapper().loadJSON(json);
			frame = new Frame(null, frame.getWrapper());

			tempFrames.add(frame);
		}

		if (callback != null) { callback.call(tempFrames, null); }
	}

	public static void smoothAll(final Animation animation, final Callback<Void> callback) {
		if (animation.getLength() < 1) { throw new IllegalArgumentException("animation too short to smooth"); }

		final int lastFrame = (animation.getLength() + 1) * getAccuracy();

		final Container<Callback<Integer>> container = new Container<>(null);
		Callback<Integer> call = new Callback<Integer>() {
			@Override
			public void call(Integer value, Throwable error) {
				if (value + getAccuracy() + 3 >= lastFrame) {
					callback.call(null, null);
					return;
				}

				smoothRange(animation, value + getAccuracy() + 1, value + getAccuracy() + 2, container.get());
			}
		};
		container.set(call);

		smoothRange(animation, 0, 1, call);

	}

	public static void smoothRange(final Animation animation, final int first, final int last, final Callback<Integer> callback) {
		AnimationSmoother smoother = new AnimationSmoother(animation.getFrame(first), animation.getFrame(last));
		smoother.setCallback(new Callback<List<Frame>>() {
			@Override
			public void call(List<Frame> value, Throwable error) {
				animation.insertFrame(first, value);

				callback.call(first, null);
			}
		});
		smoother.perform();
	}

}