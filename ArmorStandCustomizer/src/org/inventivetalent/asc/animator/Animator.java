/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.animator;

import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.chat.ChatAPI;
import org.json.JSONObject;

public class Animator {

	private final Session session;

	private Animation animation;

	private JSONObject currentState;

	public Animator(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return session;
	}

	public boolean isAnimating() {
		return getAnimation() != null;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}

	public Animation getAnimation() {
		return animation;
	}

	public void saveCurrentState() {
		if (session.isEditing()) {
			saveCurrentState(session.getEditing().toJSON());
		}
	}

	public void saveCurrentState(JSONObject state) {
		currentState = state;
	}

	public void loadCurrentState() {
		if (currentState != null) {
			if (session.isEditing()) {
				if (animation != null) {
					getAnimation().getFrame(animation.getCurrentFrame()).getWrapper().loadJSON(currentState);
				}
			}
		}
	}

	public void sendChatInterface() {
		if (!getSession().getPlayer().isOnline()) {
			return;
		}
		if (getAnimation() == null) {
			return;
		}

		boolean isFirstFrame = getAnimation().getLength() == 0 || getAnimation().getCurrentFrame() == 0;
		boolean isLastFrame = getAnimation().getLength() == 0 || getAnimation().getCurrentFrame() == getAnimation().getLength() - 1;

		String prefix = "{\"text\":\"%1$s\"},";
		String separator = "{\"text\":\"|\",\"color\":\"dark_gray\",\"bold\":\"true\"},";

		String firstFrame = "{\"text\":\" [<<] \",\"color\":\"" + (!isFirstFrame ? "red" : "dark_gray") + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate firstframe\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"First Frame\",\"color\":\"gray\"}]}},\"bold\":\"false\"},";
		String prevFrame = "{\"text\":\" [<]\",\"color\":\"" + (!isFirstFrame ? "red" : "dark_gray") + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate prevframe\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Previous Frame\",\"color\":\"gray\"}]}}},";

		String frameNumber = "{\"text\":\" < #%2$s >\",\"color\":\"gray\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Frame #%2$s\",\"color\":\"gray\"}]}}},";

		String nextFrame = "{\"text\":\" [>]\",\"color\":\"" + (!isLastFrame ? "green" : "dark_gray") + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate nextframe\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Next Frame\",\"color\":\"gray\"}]}}},";
		String lastFrame = "{\"text\":\" [>>] \",\"color\":\"" + (!isLastFrame ? "green" : "dark_gray") + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate lastframe\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Last Frame\",\"color\":\"gray\"}]}}},";

		String resetFrame = "{\"text\":\" [*] \",\"color\":\"gray\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate resetframe\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Reset Frame (State before changing the displayed frame)\",\"color\":\"gray\"}]}}}";

		String frameControls = prefix + firstFrame + prevFrame + frameNumber + nextFrame + lastFrame + separator + resetFrame;
		frameControls = String.format(frameControls, ArmorStandCustomizer.getPrefix(), getAnimation().getCurrentFrame() + 1);

		//**********************//

		boolean isMinInterval = getAnimation().getInterval() == 1;

		String frameInterval = "{\"text\":\" Interval: %2$s \",\"color\":\"gray\",\"bold\":\"false\"},";
		String decreaseInterval = "{\"text\":\" [˅]\",\"color\":\"" + (!isMinInterval ? "red" : "dark_gray") + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate interval decrease\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Decrease the delay\",\"color\":\"gray\"}]}},\"bold\":\"false\"},";
		String increaseInterval = "{\"text\":\" [˄]\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate interval increase\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Increase the delay\",\"color\":\"gray\"}]}}}";

		String intervalControls = prefix + frameInterval + separator + decreaseInterval + increaseInterval;
		intervalControls = String.format(intervalControls, ArmorStandCustomizer.getPrefix(), getAnimation().getInterval());

		//**********************//

		boolean isMinFrames = getAnimation().getLength() == 0;

		String framesInfo = "{\"text\":\" %2$s Frames \",\"color\":\"gray\",\"bold\":\"false\"},";

		String removeFrame = "{\"text\":\" [-]\",\"color\":\"" + (!isMinFrames ? "red" : "dark_gray") + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate removeframe\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Remove current frame (#%3$s)\",\"color\":\"gray\"}]}},\"bold\":\"false\"},";
		String addFrame = "{\"text\":\" [+]\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate addframe\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Add new frame (with the current pose)\",\"color\":\"gray\"}]}}},";
		String insertFrame = "{\"text\":\" [|+]\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/ascanimate insertFrame\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Insert Frame at the current position\",\"color\":\"gray\"}]}}}";

		String frameAddRemove = prefix + framesInfo + separator + removeFrame + addFrame + insertFrame;
		frameAddRemove = String.format(frameAddRemove, ArmorStandCustomizer.getPrefix(), getAnimation().getLength(), getAnimation().getCurrentFrame() + 1);

		//**********************//

		getSession().getPlayer().getPlayer().sendMessage(" ");
		ChatAPI.sendRawMessage(getSession().getPlayer().getPlayer(), "{\"text\":\"\",\"extra\":[" + frameAddRemove + "]}");
		ChatAPI.sendRawMessage(getSession().getPlayer().getPlayer(), "{\"text\":\"\",\"extra\":[" + intervalControls + "]}");
		ChatAPI.sendRawMessage(getSession().getPlayer().getPlayer(), "{\"text\":\"\",\"extra\":[" + frameControls + "]}");
	}

}
