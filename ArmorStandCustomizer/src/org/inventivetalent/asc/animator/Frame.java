/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.animator;

import org.bukkit.Location;
import org.bukkit.util.EulerAngle;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.stand.ArmorStandManager;
import org.inventivetalent.asc.stand.ArmorStandPart;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.util.JSONSerializer;
import org.json.JSONArray;
import org.json.JSONObject;

public class Frame {

	//TODO:
	//- Option to include blocks in animation

	private         Animation  animation;
	protected final JSONObject data;
	private         Location   origin;

	public Frame(Animation animation, ArmorStandWrapper wrapper) {
		this(animation, wrapper.toJSON(), null);
	}

	public Frame(Animation animation, JSONObject json, Location origin) {
		this.animation = animation;
		this.data = json;
		this.origin = origin;
		if (origin != null) {
			Location relative = json.has("relativeLocation") ? JSONSerializer.JSONToLocation(json.getJSONObject("relativeLocation")) : origin;

			Location location = new Location(relative.getWorld(), (origin.getX() + relative.getX()), (origin.getY() + relative.getY()), (origin.getZ() + relative.getZ()), relative.getYaw(), relative.getPitch());

			if (location.getWorld() != null) { setLocation(location); }
		}

		ArmorStandManager manager = ArmorStandCustomizer.getInstance().getArmorStandManager();
		if (!manager.isRegistered(getIdentifier())) {
			ArmorStandWrapper wrapper = ArmorStandWrapper.createArmorStand(origin != null ? getLocation() : null, null, getIdentifier());
			data.put("id", wrapper.getIdentifier());
		}
	}

	public JSONObject getData() {
		return data;
	}

	public String getIdentifier() {
		return data.getString("id");
	}

	public Location getLocation() {
		return JSONSerializer.JSONToLocation(data.getJSONObject("location"));
	}

	public void setLocation(Location location) {
		data.put("location", JSONSerializer.LocationToJSON(location));
	}

	public EulerAngle getPose(ArmorStandPart part) {
		JSONArray array = data.getJSONArray("poses");
		for (int i = 0; i < array.length(); i++) {
			JSONObject object = array.getJSONObject(i);
			if (object.getString("type").equals(part.name())) {
				return JSONSerializer.JSONToEulerAngle(object.getJSONObject("angle"));
			}
		}
		return null;
	}

	public void setPose(ArmorStandPart part, EulerAngle angle) {
		JSONArray array = data.getJSONArray("poses");
		JSONArray newArray = new JSONArray();
		for (int i = 0; i < array.length(); i++) {
			JSONObject object = array.getJSONObject(i);
			if (!object.getString("type").equals(part.name())) {
				newArray.put(object);
			}
		}
		JSONObject object = new JSONObject();
		object.put("type", part.name());
		object.put("angle", JSONSerializer.EulerAngleToJSON(angle));
		array.put(object);

		data.put("poses", array);
	}

	public ArmorStandWrapper getWrapper() {
		ArmorStandManager manager = ArmorStandCustomizer.getInstance().getArmorStandManager();
		String id = getIdentifier();
		if (manager.isRegistered(id)) {
			ArmorStandWrapper wrapper = manager.getArmorStand(getLocation().getWorld(), id);
			if (wrapper != null) {
				return wrapper;
			}
		}
		return null;
	}

	public JSONObject toJSON(Location origin, JSONObject previousFrame) {
		if (origin != null) {
			//Update the relative location
			Location absolute = getLocation();
			Location relative = data.has("relativeLocation") ? JSONSerializer.JSONToLocation(data.getJSONObject("relativeLocation")) : new Location(origin.getWorld(), 0, 0, 0);
			if (!relative.getWorld().equals(origin.getWorld())) {
				throw new IllegalArgumentException("cannot serialize for different world");
			}
			relative.setX(absolute.getX() - origin.getX());
			relative.setY(absolute.getY() - origin.getY());
			relative.setZ(absolute.getZ() - origin.getZ());
			relative.setYaw(absolute.getYaw());
			relative.setPitch(absolute.getPitch());

			data.put("relativeLocation", JSONSerializer.LocationToJSON(relative));
		}

		//		if (previousFrame != null) {
		//			return JSONSerializer.filterJSON(previousFrame, data);
		//		}

		return data;
	}

}
