/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.animator;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitTask;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.stand.ArmorStandWrapper;
import org.inventivetalent.asc.util.JSONSerializer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class Animation implements Runnable {

	private int interval = 10;
	private final String identifier;
	private final List<Frame> frames       = new ArrayList<>();
	private       int         currentFrame = 0;

	private boolean autoplay;
	private int     taskId;
	private boolean running;

	public Animation(String identifier) {
		this.identifier = identifier;
	}

	@Override
	public void run() {
		if (Bukkit.getOnlinePlayers().isEmpty()) { return; }
		if (getCurrentFrame() < getLength() - 1) {
			setCurrentFrame(getCurrentFrame() + 1);
		} else {
			setCurrentFrame(0);
		}
	}

	public String getIdentifier() {
		return identifier;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean flag) {
		if (flag) {
			if (isRunning()) {
				throw new IllegalStateException("already running");
			}
			BukkitTask task = Bukkit.getScheduler().runTaskTimer(ArmorStandCustomizer.getInstance(), this, interval, interval);
			taskId = task.getTaskId();
		} else {
			if (!isRunning()) {
				throw new IllegalStateException("not running");
			}
			Bukkit.getScheduler().cancelTask(taskId);
		}
		running = flag;
	}

	public void update() {
		if (isRunning()) {
			setRunning(false);
			setRunning(true);
		}
	}

	public World getWorld() {
		if (!frames.isEmpty()) {
			return frames.get(0).getLocation().getWorld();
		}
		return null;
	}

	public void setAutoplay(boolean autoplay) {
		this.autoplay = autoplay;
	}

	public boolean isAutoplay() {
		return autoplay;
	}

	public Frame getFrame(int index) {
		return frames.get(index);
	}

	public Frame setFrame(int index, ArmorStandWrapper wrapper) {
		return setFrame(index, new Frame(this, wrapper));
	}

	public Frame setFrame(int index, Frame frame) {
		if (index >= getLength()) {
			addFrame(frame);
		}
		return frames.set(index, frame);
	}

	public boolean addFrame(ArmorStandWrapper wrapper) {
		return addFrame(new Frame(this, wrapper));
	}

	public boolean addFrame(Frame frame) {
		if (frames.add(frame)) {
			if (getLength() > 1) {
				setCurrentFrame(getCurrentFrame() + 1);
			}
			return true;
		}
		return false;
	}

	public void insertFrame(int index, Collection<Frame> frame) {
		frames.addAll(index, frame);
		if (getLength() > 1) {
			setCurrentFrame(getCurrentFrame() + 1);
		}
	}

	public void insertFrame(int index, ArmorStandWrapper wrapper) {
		insertFrame(index + 1, Arrays.asList(new Frame(this, wrapper)));
	}

	public Frame removeFrame(int index) {
		if (index == getCurrentFrame()) {
			if (getLength() > 1) { setCurrentFrame(getCurrentFrame() - 1); }
		}
		return frames.remove(index);
	}

	public void removeCurrentFrame() {
		if (getLength() == 0) {
			throw new IllegalStateException("cannot remove frame (length == 0)");
		}
		removeFrame(currentFrame);
	}

	public boolean containsFrame(Frame frame) {
		return frames.contains(frame);
	}

	public int getLength() {
		return frames.size();
	}

	public void reset() {
		frames.clear();
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public int getCurrentFrame() {
		return currentFrame;
	}

	public boolean setCurrentFrame(int currentFrame) {
		if (currentFrame == this.currentFrame) { return false; }
		this.currentFrame = currentFrame;
		Frame frame = getFrame(getCurrentFrame());

		if (frame.getWrapper() != null) { frame.getWrapper().loadJSON(frame.getData()); }

		return true;
	}

	public JSONObject toJSON(Location origin) {
		JSONObject json = new JSONObject();

		json.put("id", getIdentifier());
		json.put("interval", interval);

		json.put("origin", JSONSerializer.LocationToJSON(origin));

		JSONArray array = new JSONArray();

		JSONObject previousFrame = null;

		for (Frame frame : frames) {
			array.put(previousFrame = frame.toJSON(origin, previousFrame));
		}
		json.put("frames", array);
		json.put("length", array.length());
		json.put("duration", array.length() * interval);

		json.put("autoplay", autoplay);

		return json;
	}

	public void loadJSON(JSONObject json, Location origin) {
		loadJSON(json, origin, false);
	}

	public void loadJSON(JSONObject json, Location origin, boolean internal) {
		this.interval = json.getInt("interval");

		JSONArray array = json.getJSONArray("frames");

		Map<String, String> replacements = new HashMap<>();
		if (!internal) {
			// Make all ids unique
			for (int i = 0; i < array.length(); i++) {
				JSONObject current = array.getJSONObject(i);
				String id = current.getString("id");
				if (!replacements.containsKey(id)) {
					replacements.put(id, ArmorStandWrapper.makeUniqueId(id));
				}
			}
		}

		for (int i = 0; i < array.length(); i++) {
			JSONObject current = array.getJSONObject(i);
			String id = current.getString("id");
			String replacement = replacements.get(id);
			current.put("id", replacement != null ? replacement : id);
			setFrame(i, new Frame(this, current, origin));
		}

		this.autoplay = json.has("autoplay") && json.getBoolean("autoplay");
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		Animation animation = (Animation) o;

		if (currentFrame != animation.currentFrame) { return false; }
		if (interval != animation.interval) { return false; }
		if (!frames.equals(animation.frames)) { return false; }
		if (!identifier.equals(animation.identifier)) { return false; }

		return true;
	}

	@Override
	public int hashCode() {
		int result = interval;
		result = 31 * result + identifier.hashCode();
		result = 31 * result + frames.hashCode();
		result = 31 * result + currentFrame;
		return result;
	}
}
