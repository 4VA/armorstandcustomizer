/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.animator;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.data.DataManager;
import org.inventivetalent.asc.data.FileResult;
import org.inventivetalent.asc.util.JSONSerializer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class AnimationManager {

	private Plugin plugin;

	private final Map<String, Animation> animations = new HashMap<>();

	public AnimationManager(Plugin plugin) {
		this.plugin = plugin;
	}

	public Animation createAnimation(String name) {
		if (isRegistered(name)) {
			throw new IllegalArgumentException("animation is already registered");
		}
		Animation animation = new Animation(name);
		animations.put(name, animation);
		return animation;
	}

	public boolean isRegistered(String name) {
		return animations.containsKey(name);
	}

	public Animation getAnimation(String name) {
		if (animations.containsKey(name)) {
			return animations.get(name);
		}
		throw new IllegalArgumentException("animation is not registered");
	}

	public Animation getAnimation(Animation animation) {
		if (animations.containsValue(animation)) {
			return animation;
		}
		throw new IllegalArgumentException("animation is not registered");
	}

	public void unregister(String name) {
		if (!isRegistered(name)) {
			throw new IllegalArgumentException("not registered");
		}
		animations.remove(name);
	}

	public FileResult saveAnimation(Animation animation, Location location) {
		DataManager dataManager = ArmorStandCustomizer.getInstance().getDataManager();
		String name = animation.getIdentifier();
		return dataManager.saveAnimation(getAnimation(animation), name, location);
	}

	public Animation loadAnimation(Location origin, JSONObject json, String id) {
		Animation animation = isRegistered(id) ? getAnimation(id) : createAnimation(id);
		animation.loadJSON(json, origin);
		return animation;
	}

	public FileResult loadAnimation(Player player, String fileName, String newName) {
		DataManager dataManager = ArmorStandCustomizer.getInstance().getDataManager();
		FileResult<Animation> result = dataManager.loadAnimation(player.getLocation(), fileName, newName);
		if (result.getResult() == FileResult.Result.SUCCESS) {
			animations.put(newName != null ? newName : result.getContent().getIdentifier(), result.getContent());
		}
		return result;
	}

	public void playAnimation(Animation animation) {
		getAnimation(animation).setRunning(true);
	}

	public void stopAnimation(Animation animation) {
		getAnimation(animation).setRunning(false);
	}

	public List<String> getAnimationNames() {
		List<String> names = new ArrayList<>(animations.keySet());
		Collections.sort(names);
		return names;
	}

	public JSONArray registrationsToJSON() {
		JSONArray array = new JSONArray();

		for (Animation animation : animations.values()) {
			if (animation.getLength() == 0) { continue; }//Don't save empty animations, since we don't have a world
			Location origin = new Location(animation.getWorld(), 0, 0, 0);

			JSONObject object = animation.toJSON(origin);

			array.put(object);
		}

		return array;
	}

	public void loadRegistrations(JSONArray array) {
		List<String> autoplay = new ArrayList<>();

		for (int i = 0; i < array.length(); i++) {
			JSONObject object = array.getJSONObject(i);

			String id = object.getString("id");
			Location origin = JSONSerializer.JSONToLocation(object.getJSONObject("origin"));

			Animation animation = createAnimation(id);
			animation.loadJSON(object, origin, true);

			if (animation.isAutoplay()) {
				autoplay.add(id);
			}

			plugin.getLogger().info("Loaded Animation \"" + id + "\"");
		}

		for (String name : autoplay) {
			playAnimation(getAnimation(name));

			plugin.getLogger().info("Auto-playing \"" + name + "\"");
		}
	}

}
