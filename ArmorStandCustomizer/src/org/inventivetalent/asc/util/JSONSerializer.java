/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.util;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class JSONSerializer {

	public static JSONObject VectorToJSON(Vector vector) {
		JSONObject json = new JSONObject();

		json.put("x", vector.getX());
		json.put("y", vector.getY());
		json.put("z", vector.getZ());

		return json;
	}

	public static Vector JSONToVector(JSONObject json) {
		double x = json.getDouble("x");
		double y = json.getDouble("y");
		double z = json.getDouble("z");

		return new Vector(x, y, z);
	}

	public static JSONObject EulerAngleToJSON(EulerAngle angle) {
		JSONObject json = new JSONObject();

		json.put("x", angle.getX());
		json.put("y", angle.getY());
		json.put("z", angle.getZ());

		return json;
	}

	public static EulerAngle JSONToEulerAngle(JSONObject json) {
		double x = json.getDouble("x");
		double y = json.getDouble("y");
		double z = json.getDouble("z");

		return new EulerAngle(x, y, z);
	}

	public static JSONObject LocationToJSON(Location location) {
		JSONObject json = VectorToJSON(location.toVector());

		json.put("world", location.getWorld().getName());
		json.put("yaw", location.getYaw());
		json.put("pitch", location.getPitch());

		return json;
	}

	public static Location JSONToLocation(JSONObject json) {
		Vector vector = JSONToVector(json);

		String worldName = json.getString("world");
		World world = Bukkit.getWorld(worldName);
		float yaw = (float) json.getDouble("yaw");
		float pitch = (float) json.getDouble("pitch");

		Location location = vector.toLocation(world);
		location.setYaw(yaw);
		location.setPitch(pitch);

		return location;
	}

	public static JSONObject BlockToJSON(Block block, Location origin) {
		JSONObject json = new JSONObject();

		Location loc = block.getLocation();
		if (origin != null) {
			loc.setX(block.getLocation().getX() - origin.getX());
			loc.setY(block.getLocation().getY() - origin.getY());
			loc.setZ(block.getLocation().getZ() - origin.getZ());
		}

		JSONObject location = LocationToJSON(loc);

		json.put("location", location);
		json.put("type", block.getTypeId());
		json.put("data", (int) block.getData());

		return json;
	}

	public static Block JSONToBlock(JSONObject json, World world, Location origin, Integer[] skipTypes) {
		Location location = JSONToLocation(json.getJSONObject("location"));
		if (world != null) { location.setWorld(world); }

		if (origin != null) {
			location.setX(location.getX() + origin.getX());
			location.setY(location.getY() + origin.getY());
			location.setZ(location.getZ() + origin.getZ());
		}

		int type = json.getInt("type");
		for (int skip : skipTypes) {
			if (skip == type) { return null; }
		}

		byte data = (byte) json.getInt("data");

		Block block = location.getBlock();
		block.setTypeIdAndData(type, data, false);
		return block;
	}

	public static JSONObject ConfigurationSerializableToJSON(ConfigurationSerializable serializable) {
		Map<String, Object> serialized = serializable.serialize();

		JSONObject json = SerializedToJSON(serialized);

		return json;
	}

	public static ItemStack JSONToItemStack(JSONObject json) {
		Map<String, Object> map = JSONToMap(json);

		ItemStack deserialized = ItemStack.deserialize(map);

		if (map.containsKey("meta")) {
			Map<String, Object> metaMap = (Map<String, Object>) map.get("meta");
			metaMap.put("==", "ItemMeta");

			Iterator<Map.Entry<String, Object>> entryIterator = metaMap.entrySet().iterator();

			Map<String, Object> toReplace = new HashMap<>();

			while (entryIterator.hasNext()) {
				Map.Entry<String, Object> entry = entryIterator.next();

				if (entry.getValue() instanceof JSONArray) {
					JSONArray array = (JSONArray) entry.getValue();
					List<Object> list = new ArrayList<>();

					for (int i = 0; i < array.length(); i++) {
						list.add(array.get(i));
					}

					toReplace.put(entry.getKey(), list);
				}
			}
			for (Map.Entry<String, Object> entry : toReplace.entrySet()) {
				metaMap.put(entry.getKey(), entry.getValue());
			}

			ItemMeta meta = null;

			if (metaMap.containsKey("color")) {
				Map<String, Object> colorMap = (Map<String, Object>) metaMap.get("color");
				colorMap.put("==", "Color");

				metaMap.remove("color");

				Color color = (Color) ConfigurationSerialization.deserializeObject(colorMap);

				meta = (ItemMeta) ConfigurationSerialization.deserializeObject(metaMap);
				((LeatherArmorMeta) meta).setColor(color);
			} else { meta = (ItemMeta) ConfigurationSerialization.deserializeObject(metaMap); }
			deserialized.setItemMeta(meta);
		}

		return deserialized;
	}

	public static JSONObject SerializedToJSON(Map<String, Object> serialized) {
		JSONObject json = new JSONObject();

		for (Map.Entry<String, Object> entry : serialized.entrySet()) {
			if (entry.getValue() instanceof ConfigurationSerializable) {
				json.put(entry.getKey(), SerializedToJSON(((ConfigurationSerializable) entry.getValue()).serialize()));
			} else {
				json.put(entry.getKey(), entry.getValue());
			}
		}

		return json;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> JSONToMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = object.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if (value instanceof JSONObject) {
				value = JSONToMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static JSONObject filterJSON(JSONObject original, JSONObject copy) {
		JSONObject different = new JSONObject();

		for (String key : (Set<String>) copy.keySet()) {
			Object value = copy.get(key);
			Object origValue = null;
			try {
				origValue = original.get(key);
			} catch (Exception e) {
			}

			if (origValue == null) {
				different.put(key, value);
				continue;
			}

			if ("uuid".equals(key) || "id".equals(key)) {
				different.put(key, value);//Always keep uuid and identifier
			}

			if (value instanceof JSONObject) {
				if (origValue instanceof JSONObject) {
					if (!((JSONObject) value).equals(origValue)) {
						different.put(key, value);
					}
				}
			}
			if (value instanceof JSONArray) {
				if (origValue instanceof JSONArray) {
					if (!((JSONArray) value).equals(origValue)) {
						different.put(key, value);
					}
				}
			}
		}

		return different;
	}

}

