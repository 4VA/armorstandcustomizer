/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.stand;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.inventivetalent.asc.session.Session;
import org.inventivetalent.asc.util.AccessUtil;
import org.inventivetalent.asc.util.JSONSerializer;
import org.inventivetalent.asc.util.Reflection;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class ArmorStandWrapper implements Comparable<ArmorStandWrapper> {

	private final ArmorStand wrapped;
	private final String     identifier;

	private Session editor;

	private ArmorStandWrapper(ArmorStand stand, String identifier) {
		this.wrapped = stand;
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setEditor(Session editor) {
		this.editor = editor;
	}

	@Nullable
	public Session getEditor() {
		return editor;
	}

	public void moveRelative(Vector move) {
		wrapped.teleport(wrapped.getLocation().add(move.getX(), move.getY(), move.getZ()));
		updateDisplay(true);
		if (getEditor() != null) { getEditor().updateInfoMessage("§2Location §9- (§b%s§9, §b%s§9, §b%s§9)", (float) wrapped.getLocation().getX(), (float) wrapped.getLocation().getY(), (float) wrapped.getLocation().getZ()); }
	}

	public void teleport(Location location) {
		if (location.getYaw() == 0) {
			location.setYaw(getWrapped().getLocation().getYaw());
		}
		if (location.getPitch() == 0) {
			location.setPitch(getWrapped().getLocation().getPitch());
		}

		getWrapped().teleport(location);
	}

	public void rotateRelative(float amount) {
		Location location = wrapped.getLocation();
		location.setYaw(location.getYaw() + amount);
		getWrapped().teleport(location);
		updateDisplay(true);
		if (getEditor() != null) { getEditor().updateInfoMessage("§2Rotation §9- (§b%s§9)", location.getYaw()); }
	}

	public void addPose(ArmorStandPart part, EulerAngle add) {
		add = new EulerAngle(Math.toRadians(add.getX()), Math.toRadians(add.getY()), Math.toRadians(add.getZ()));

		setPose(part, getPose(part).add(add.getX(), add.getY(), add.getZ()));
	}

	public void subtractPose(ArmorStandPart part, EulerAngle subtract) {
		subtract = new EulerAngle(Math.toRadians(subtract.getX()), Math.toRadians(subtract.getY()), Math.toRadians(subtract.getZ()));
		setPose(part, getPose(part).subtract(subtract.getX(), subtract.getY(), subtract.getZ()));
	}

	public void setPose(ArmorStandPart part, EulerAngle pose) {
		switch (part) {
			case HEAD:
				wrapped.setHeadPose(pose);
				break;
			case BODY:
				wrapped.setBodyPose(pose);
				break;
			case LEFT_ARM:
				wrapped.setLeftArmPose(pose);
				break;
			case RIGHT_ARM:
				wrapped.setRightArmPose(pose);
				break;
			case LEFT_LEG:
				wrapped.setLeftLegPose(pose);
				break;
			case RIGHT_LEG:
				wrapped.setRightLegPose(pose);
				break;
		}
		updateDisplay();
		if (getEditor() != null) { getEditor().updateInfoMessage("§2%s §9- (§b%s§9, §b%s§9, §b%s§9)", part.toPlainText(), (float) pose.getX(), (float) pose.getY(), (float) pose.getZ()); }
	}

	public EulerAngle getPose(ArmorStandPart part) {
		switch (part) {
			case HEAD:
				return wrapped.getHeadPose();
			case BODY:
				return wrapped.getBodyPose();
			case LEFT_ARM:
				return wrapped.getLeftArmPose();
			case RIGHT_ARM:
				return wrapped.getRightArmPose();
			case LEFT_LEG:
				return wrapped.getLeftLegPose();
			case RIGHT_LEG:
				return wrapped.getRightLegPose();
		}
		throw new IllegalArgumentException("Unknown ArmorStandPart: " + part);
	}

	public void setItem(ArmorStandPart part, org.bukkit.inventory.ItemStack item) {
		ArmorStandSlot slot = part.getSlot();
		if (slot == null) {
			throw new IllegalArgumentException(part + " cannot hold an item");
		}
		slot.setItem(getWrapped(), item);

		if (getEditor() != null) {
			getEditor().sendMessage("§aSet item for §2%s§a to §2%s:%s§a.", part.toPlainText(), item.getType(), item.getDurability());
		}
	}

	public org.bukkit.inventory.ItemStack getItem(ArmorStandPart part) {
		ArmorStandSlot slot = part.getSlot();
		if (slot == null) {
			throw new IllegalArgumentException(part + " cannot hold an item");
		}
		return slot.getItem(getWrapped());
	}

	public void updateDisplay() {
		updateDisplay(false);
	}

	public void updateDisplay(boolean hard) {
		try {
			Object dataWatcher = Reflection.getNMSClass("Entity").getDeclaredMethod("getDataWatcher").invoke(Reflection.getHandle(getWrapped()));
			Object metaPacket = Reflection.getNMSClass("PacketPlayOutEntityMetadata").getConstructor(int.class, Reflection.getNMSClass("DataWatcher"), boolean.class).newInstance(getWrapped().getEntityId(), dataWatcher, false);

			//			int locX = (int) ((getWrapped().getLocation().getX()) * 32.0D);
			//			int locY = (int) ((getWrapped().getLocation().getY()) * 32.0D);
			//			int locZ = (int) ((getWrapped().getLocation().getZ()) * 32.0D);
			//
			//			byte yaw = (byte) ((int) (getWrapped().getLocation().getYaw() * 256.0F / 360.0F));
			//			byte pitch = (byte) ((int) getWrapped().getLocation().getPitch() * 256.0F / 360.0F);
			//
			//			AccessUtil.setAccessible(teleportPacket.getClass().getDeclaredField("b")).set(teleportPacket, locX);
			//			AccessUtil.setAccessible(teleportPacket.getClass().getDeclaredField("c")).set(teleportPacket, locY);
			//			AccessUtil.setAccessible(teleportPacket.getClass().getDeclaredField("d")).set(teleportPacket, locZ);
			//			AccessUtil.setAccessible(teleportPacket.getClass().getDeclaredField("e")).set(teleportPacket, yaw);
			//			AccessUtil.setAccessible(teleportPacket.getClass().getDeclaredField("f")).set(teleportPacket, pitch);

			getWrapped().eject();
			for (Player p : Bukkit.getOnlinePlayers()) {
				if (!p.getWorld().equals(wrapped.getWorld())) {
					continue;
				}
				Reflection.sendPacket(p, metaPacket);
			}
			if (hard && getEditor() != null) {
				Bukkit.getScheduler().runTaskLater(ArmorStandCustomizer.getInstance(), new Runnable() {
					@Override
					public void run() {
						for (final Player p : Bukkit.getOnlinePlayers()) {
							if (!p.getWorld().equals(wrapped.getWorld())) {
								continue;
							}
							try {
								final Object teleportPacket = Reflection.getNMSClass("PacketPlayOutEntityTeleport").getConstructor(Reflection.getNMSClass("Entity")).newInstance(Reflection.getHandle(getWrapped()));
								Reflection.sendPacket(p, teleportPacket);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}, 10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setInvulnerable(boolean flag) {
		Class clazz = Reflection.getNMSClass("Entity");
		try {
			AccessUtil.setAccessible(clazz.getDeclaredField("invulnerable")).setBoolean(Reflection.getHandle(getWrapped()), flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateAttributes(List<ArmorStandAttribute> attributes) {
		if (attributes != null && !attributes.isEmpty()) {
			for (ArmorStandAttribute attribute : attributes) {
				attribute.applyTo(getWrapped());
				if (getEditor() != null) { getEditor().sendMessage("§aUpdated attribute §7%s", attribute.name()); }
			}
		}
		updateDisplay();
	}

	public List<ArmorStandAttribute> getAttributes() {
		return ArmorStandAttribute.getFor(getWrapped());
	}

	public void resetPosition() {
		for (ArmorStandPart part : ArmorStandPart.values()) {
			setPose(part, EulerAngle.ZERO);
		}
		Location location = getWrapped().getLocation();
		location.setYaw(0);
		location.setPitch(0);
		getWrapped().teleport(location);
		updateDisplay();
	}

	public JSONObject toJSON() {
		return toJSON(null);
	}

	public JSONObject toJSON(Location origin) {
		JSONObject json = new JSONObject();

		json.put("id", getIdentifier());
		json.put("uuid", getWrapped().getUniqueId().toString());
		json.put("name", getWrapped().getCustomName());

		JSONArray poses = new JSONArray();
		JSONArray items = new JSONArray();

		for (ArmorStandPart part : ArmorStandPart.values()) {
			if (part == ArmorStandPart.NOT_SET || part == ArmorStandPart.FEET) { continue; }
			try {
				JSONObject pose = new JSONObject();
				pose.put("type", part.name());
				pose.put("angle", JSONSerializer.EulerAngleToJSON(getPose(part)));

				poses.put(pose);

				if (part.hasItem()) {
					org.bukkit.inventory.ItemStack partItem = getItem(part);
					if (partItem != null) {
						JSONObject item = JSONSerializer.ConfigurationSerializableToJSON(partItem);

						JSONObject itemObject = new JSONObject();
						itemObject.put("slot", part.name());
						itemObject.put("item", item);

						items.put(itemObject);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		json.put("poses", poses);
		json.put("items", items);

		JSONArray attributes = new JSONArray();
		for (ArmorStandAttribute attribute : getAttributes()) {
			attributes.put(attribute.name());
		}
		json.put("attributes", attributes);

		json.put("location", JSONSerializer.LocationToJSON(getWrapped().getLocation()));

		if (origin != null) {
			if (!origin.getWorld().equals(getWrapped().getWorld())) {
				throw new IllegalArgumentException("Cannot set relative location for different worlds");
			}

			Location location = getWrapped().getLocation();

			Location relative = new Location(origin.getWorld(), (location.getX() - origin.getX()), (location.getY() - origin.getY()), (location.getZ() - origin.getZ()), location.getYaw(), location.getPitch());

			json.put("relativeLocation", JSONSerializer.LocationToJSON(relative));
		}

		return json;
	}

	public void loadJSON(JSONObject json) {
		loadJSON(json, null);
	}

	public void loadJSON(JSONObject json, Location origin) {
		if (json.has("name")) {
			getWrapped().setCustomName(json.getString("name"));
		}

		if (json.has("poses")) {
			JSONArray poses = json.getJSONArray("poses");
			for (int i = 0; i < poses.length(); i++) {
				JSONObject pose = poses.getJSONObject(i);

				ArmorStandPart type = ArmorStandPart.valueOf(pose.getString("type"));
				EulerAngle angle = JSONSerializer.JSONToEulerAngle(pose.getJSONObject("angle"));

				if (type != null && angle != null) { setPose(type, angle); }
			}
		}

		if (json.has("items")) {
			JSONArray items = json.getJSONArray("items");
			for (int i = 0; i < items.length(); i++) {
				JSONObject item = items.getJSONObject(i);

				ArmorStandPart slot = ArmorStandPart.valueOf(item.getString("slot"));
				org.bukkit.inventory.ItemStack itemStack = JSONSerializer.JSONToItemStack(item.getJSONObject("item"));

				if (slot != null) { setItem(slot, itemStack); }
			}
		}

		if (json.has("attributes")) {
			JSONArray attributes = json.getJSONArray("attributes");
			List<ArmorStandAttribute> attributeList = new ArrayList<>();
			for (int i = 0; i < attributes.length(); i++) {
				attributeList.add(ArmorStandAttribute.valueOf(attributes.getString(i)));
			}
			updateAttributes(attributeList);
		}

		if (origin != null) {
			if (json.has("relativeLocation")) {
				Location relative = JSONSerializer.JSONToLocation(json.getJSONObject("relativeLocation"));

				Location location = new Location(relative.getWorld(), (origin.getX() + relative.getX()), (origin.getY() + relative.getY()), (origin.getZ() + relative.getZ()), relative.getYaw(), relative.getPitch());

				getWrapped().teleport(location);

				updateDisplay(true);
				return;
			} else {
				throw new IllegalArgumentException("JSON does not contain relative location");
			}
		}

		if (json.has("location")) {
			Location location = JSONSerializer.JSONToLocation(json.getJSONObject("location"));
			getWrapped().teleport(location);
		}

		updateDisplay(true);
	}

	public ArmorStand getWrapped() {
		return wrapped;
	}

	public static ArmorStandWrapper wrap(ArmorStand stand, String identifier) {
		ArmorStandWrapper wrapper = new ArmorStandWrapper(stand, identifier);

		wrapper.getWrapped().setArms(true);
		wrapper.setInvulnerable(true);
		wrapper.getWrapped().setGravity(false);

		ArmorStandManager manager = ArmorStandCustomizer.getInstance().getArmorStandManager();
		if (!manager.isRegistered(wrapper.getWrapped().getUniqueId())) {
			manager.register(wrapper);
		}

		stand.setMetadata("ASC_ID", new FixedMetadataValue(ArmorStandCustomizer.getInstance(), identifier));

		return wrapper;
	}

	public static ArmorStandWrapper createArmorStand(Location location, Session editor, String identifier) {
		location = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());//remove pitch and yaw

		ArmorStand stand = location.getWorld().spawn(location, ArmorStand.class);
		ArmorStandWrapper wrapper = wrap(stand, identifier);
		wrapper.setEditor(editor);

		wrapper.resetPosition();

		return wrapper;
	}

	public static ArmorStandWrapper load(JSONObject json, Session editor, Location origin) {
		return load(json, editor, origin, null);
	}

	public static ArmorStandWrapper load(final JSONObject json, Session editor, final Location origin, String id) {
		if (id == null) {
			id = json.getString("id");
		}
		id = makeUniqueId(id);

		ArmorStand stand = origin.getWorld().spawn(origin, ArmorStand.class);

		if (stand.getCustomName() == null || stand.getCustomName().isEmpty()) {
			stand.setCustomName(id);
		}

		final ArmorStandWrapper wrapper = wrap(stand, id);
		wrapper.setEditor(editor);

		wrapper.loadJSON(json, origin);

		return wrapper;
	}

	public static String makeUniqueId(String original) {
		ArmorStandManager manager = ArmorStandCustomizer.getInstance().getArmorStandManager();

		if (!manager.isRegistered(original)) {
			return original;
		}

		int currentId = 1;

		if (original.length() > 1) {
			char lastChar = original.charAt(original.length() - 1);
			try {
				currentId = Integer.parseInt(String.valueOf(lastChar));
				original = original.substring(0, original.length() - 1);
			} catch (NumberFormatException e) {}
		}

		String current = original;

		while (manager.isRegistered(current)) {
			current = original + currentId;
			currentId++;
		}

		return current;
	}

	@Override
	public int compareTo(ArmorStandWrapper o) {
		if (o == null) { return 0; }
		return getIdentifier().compareTo(o.getIdentifier());
	}
}
