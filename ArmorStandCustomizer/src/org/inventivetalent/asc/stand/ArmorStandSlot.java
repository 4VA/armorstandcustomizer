/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.stand;

import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import org.inventivetalent.reflection.minecraft.Minecraft;
import org.inventivetalent.reflection.resolver.MethodResolver;
import org.inventivetalent.reflection.resolver.ResolverQuery;
import org.inventivetalent.reflection.resolver.minecraft.NMSClassResolver;
import org.inventivetalent.reflection.resolver.minecraft.OBCClassResolver;

public enum ArmorStandSlot {

	HEAD,
	LEFT_HAND,
	RIGHT_HAND,
	BODY,
	LEGS,
	FEET;

	public void setItem(ArmorStand stand, ItemStack item) {
		switch (this) {
			case HEAD:
				stand.setHelmet(item);
				break;
			case LEFT_HAND:
				if (Minecraft.VERSION.olderThan(Minecraft.Version.v1_9_R1)) {
					throw new UnsupportedOperationException("Cannot set item in left hand. Server version is < 1.9");
				}
				//Apparently there's no API method for the ArmorStand OffHand yet...
				try {
					Object equipment = CraftLivingEntityMethodResolver.resolve(new ResolverQuery("getEquipment")).invoke(stand);
					CraftEntityEquipmentMethodResolver.resolve(new ResolverQuery("setItemInOffHand", ItemStack.class)).invoke(equipment, item);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case RIGHT_HAND:
				stand.setItemInHand(item);
				break;
			case BODY:
				stand.setChestplate(item);
				break;
			case LEGS:
				stand.setLeggings(item);
				break;
			case FEET:
				stand.setBoots(item);
				break;
		}
	}

	public ItemStack getItem(ArmorStand stand) {
		switch (this) {
			case HEAD:
				return stand.getHelmet();
			case LEFT_HAND:
				if (Minecraft.VERSION.olderThan(Minecraft.Version.v1_9_R1)) {
					throw new UnsupportedOperationException("Cannot get item in left hand. Server version is < 1.9");
				}
				try {
					Object equipment = CraftLivingEntityMethodResolver.resolve(new ResolverQuery("getEquipment")).invoke(stand);
					return (ItemStack) CraftEntityEquipmentMethodResolver.resolve(new ResolverQuery("getItemInOffHand")).invoke(equipment);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case RIGHT_HAND:
				return stand.getItemInHand();
			case BODY:
				return stand.getChestplate();
			case LEGS:
				return stand.getLeggings();
			case FEET:
				return stand.getBoots();
		}
		return null;
	}

	static NMSClassResolver nmsClassResolver                   = new NMSClassResolver();
	static OBCClassResolver obcClassResolver                   = new OBCClassResolver();
	static Class<?>         CraftArmorStand                    = obcClassResolver.resolveSilent("entity.CraftArmorStand");
	static Class<?>         CraftLivingEntity                  = obcClassResolver.resolveSilent("entity.CraftLivingEntity");
	static Class<?>         CraftEntityEquipment               = obcClassResolver.resolveSilent("inventory.CraftEntityEquipment");
	static MethodResolver   CraftEntityEquipmentMethodResolver = new MethodResolver(CraftEntityEquipment);
	static MethodResolver   CraftArmorStandMethodResolver      = new MethodResolver(CraftArmorStand);
	static MethodResolver   CraftLivingEntityMethodResolver    = new MethodResolver(CraftLivingEntity);

}
