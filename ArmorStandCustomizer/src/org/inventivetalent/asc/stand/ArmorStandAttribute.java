/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.stand;

import org.bukkit.entity.ArmorStand;

import java.util.ArrayList;
import java.util.List;

public enum ArmorStandAttribute {

	BIG,
	SMALL,
	VISIBLE,
	INVISIBLE,
	GRAVITY,
	NO_GRAVITY,
	BASE_PLATE,
	NO_BASE_PLATE,
	NAMETAG_VISIBLE,
	NAMETAG_INVISIBLE;

	private ArmorStandAttribute() {
	}

	public void applyTo(ArmorStand stand) {
		switch (this) {
			case BIG:
				stand.setSmall(false);
				break;
			case SMALL:
				stand.setSmall(true);
				break;
			case VISIBLE:
				stand.setVisible(true);
				break;
			case INVISIBLE:
				stand.setVisible(false);
				break;
			case GRAVITY:
				stand.setGravity(true);
				break;
			case NO_GRAVITY:
				stand.setGravity(false);
				break;
			case BASE_PLATE:
				stand.setBasePlate(true);
				break;
			case NO_BASE_PLATE:
				stand.setBasePlate(false);
				break;
			case NAMETAG_VISIBLE:
				stand.setCustomNameVisible(true);
				break;
			case NAMETAG_INVISIBLE:
				stand.setCustomNameVisible(false);
				break;
			default:
				throw new IllegalArgumentException("Could not apply " + name() + " to " + stand);
		}
	}

	public static List<ArmorStandAttribute> getFor(ArmorStand stand) {
		List<ArmorStandAttribute> attributes = new ArrayList<>();

		if (stand.isSmall()) {
			attributes.add(SMALL);
		} else {
			attributes.add(BIG);
		}

		if (stand.isVisible()) {
			attributes.add(VISIBLE);
		} else {
			attributes.add(INVISIBLE);
		}

		if (stand.hasGravity()) {
			attributes.add(GRAVITY);
		} else {
			attributes.add(NO_GRAVITY);
		}

		if (stand.hasBasePlate()) {
			attributes.add(BASE_PLATE);
		} else {
			attributes.add(NO_BASE_PLATE);
		}

		if (stand.isCustomNameVisible()) {
			attributes.add(NAMETAG_VISIBLE);
		} else {
			attributes.add(NAMETAG_INVISIBLE);
		}

		return attributes;
	}

}
