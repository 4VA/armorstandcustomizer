/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.stand;

import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.inventivetalent.asc.ArmorStandCustomizer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class ArmorStandManager {

	private ArmorStandCustomizer plugin;

	private final Map<UUID, String> armorStands = new HashMap<>();

	public ArmorStandManager(ArmorStandCustomizer plugin) {
		this.plugin = plugin;
	}

	public void register(ArmorStandWrapper wrapper) {
		if (wrapper == null) {
			throw new IllegalArgumentException("stand cannot be null");
		}
		if (isRegistered(wrapper.getWrapped().getUniqueId())) {
			throw new IllegalStateException(wrapper.getIdentifier() + " (" + wrapper.getWrapped().getUniqueId() + ") is already registered");
		}

		armorStands.put(wrapper.getWrapped().getUniqueId(), wrapper.getIdentifier());
	}

	public void unregister(UUID uuid) {
		if (!isRegistered(uuid)) {
			throw new IllegalArgumentException(uuid + " is not registered");
		}

		armorStands.remove(uuid);
	}

	public boolean isRegistered(ArmorStand stand) {
		return stand.hasMetadata("ASC_WRAPPER") || isRegistered(stand.getUniqueId());
	}

	public boolean isRegistered(UUID uuid) {
		return armorStands.containsKey(uuid);
	}

	public boolean isRegistered(String identifier) {
		return armorStands.containsValue(identifier);
	}

	public String getIdentifier(UUID uuid) {
		if (!isRegistered(uuid)) {
			throw new IllegalArgumentException(uuid + " is not registered");
		}
		return armorStands.get(uuid);
	}

	public UUID getUUID(String identifier) {
		if (!isRegistered(identifier)) {
			throw new IllegalArgumentException(identifier + " is not registered");
		}
		for (Map.Entry<UUID, String> entry : getRegistrations().entrySet()) {
			if (entry.getValue().equals(identifier)) {
				return entry.getKey();
			}
		}
		return null;
	}

	public ArmorStandWrapper getArmorStand(World world, String identifier) {
		return getArmorStand(world, getUUID(identifier), identifier);
	}

	public ArmorStandWrapper getArmorStand(World world, UUID uuid, String identifier) {
		Iterator<ArmorStand> iterator = world.getEntitiesByClass(ArmorStand.class).iterator();
		while (iterator.hasNext()) {
			ArmorStand next = iterator.next();

			if (next.getUniqueId().equals(uuid)) {
				return ArmorStandWrapper.wrap(next, identifier != null ? identifier : getIdentifier(uuid));
			}
		}
		return null;
	}

	public Map<UUID, String> getRegistrations() {
		return armorStands;
	}

	public Collection<UUID> getRegisteredUUIDs() {
		return armorStands.keySet();
	}

	public Collection<String> getRegisteredIdentifiers() {
		List<String> ids = new ArrayList<>(armorStands.values());
		Collections.sort(ids);
		return ids;
	}

	public JSONArray registrationsToJSON() {
		JSONArray array = new JSONArray();

		for (Map.Entry<UUID, String> entry : getRegistrations().entrySet()) {
			JSONObject object = new JSONObject();
			object.put("uuid", entry.getKey().toString());
			object.put("id", entry.getValue());

			array.put(object);
		}

		return array;
	}

	public void loadRegistrations(JSONArray array) {
		for (int i = 0; i < array.length(); i++) {
			JSONObject object = array.getJSONObject(i);

			UUID uuid = UUID.fromString(object.getString("uuid"));
			String id = object.getString("id");

			armorStands.put(uuid, id);
			plugin.getLogger().info("Loaded Armorstand \"" + id + "\"");
		}
	}

}
