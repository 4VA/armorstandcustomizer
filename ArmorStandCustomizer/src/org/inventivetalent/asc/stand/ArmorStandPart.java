/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.asc.stand;

public enum ArmorStandPart {

	HEAD("head", true, ArmorStandSlot.HEAD),
	BODY("body", true, ArmorStandSlot.BODY),
	LEFT_ARM("left arm", true, ArmorStandSlot.LEFT_HAND),
	RIGHT_ARM("right arm", true, ArmorStandSlot.RIGHT_HAND),
	LEFT_LEG("left leg", true, ArmorStandSlot.LEGS),
	RIGHT_LEG("right leg", true, ArmorStandSlot.LEGS),
	FEET("feet", true, ArmorStandSlot.FEET),
	NOT_SET("not set");

	private String         text;
	private boolean        hasItem;
	private ArmorStandSlot slot;

	private ArmorStandPart(String text) {
		this(text, false, null);
	}

	private ArmorStandPart(String text, boolean hasItem, ArmorStandSlot slot) {
		this.text = text;
		this.hasItem = hasItem;
		this.slot = slot;
	}

	public String toPlainText() {
		return text;
	}

	public boolean hasItem() {
		return hasItem;
	}

	public ArmorStandSlot getSlot() {
		if (!hasItem()) {
			return null;
		}
		return slot;
	}

}
